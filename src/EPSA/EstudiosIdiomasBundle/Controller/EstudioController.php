<?php

namespace EPSA\EstudiosIdiomasBundle\Controller;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use EPSA\EstudiosIdiomasBundle\Entity\Estudio;
use EPSA\EstudiosIdiomasBundle\Form\EstudioType;

/**
 * Estudio controller.
 *
 */
class EstudioController extends Controller
{
    /**
     * Creates a new estudio entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $estudios = $em->getRepository('EstudiosIdiomasBundle:Estudio')->findAll();

        $estudio = new Estudio();
        $form = $this->createForm(EstudioType::class, $estudio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($estudio);
                $em->flush($estudio);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The new study has been created correctly.'
                    )
                );
                return $this->redirectToRoute('estudio_new');

            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'There is already a study "' . $form->get('estudio')->getData() . '" almacenado.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );
                $message = 'There is already a study "' . $form->get('estudio')->getData() . '" almacenado.';
                $form->get('estudio')->addError(new FormError($message));
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger',
                        'message' => $message
                    )
                );
                $form->get('estudio')->addError(new FormError($message));
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger',
                        'message' => $message
                    )
                );
                $form->get('estudio')->addError(new FormError($message));
            }
        }

        return $this->render('@EstudiosIdiomas/estudio/new.html.twig', array(
            'estudio' => $estudio,
            'estudios' => $estudios,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'New',
        ));
    }

    /**
     * Displays a form to edit an existing estudio entity.
     *
     */
    public function editAction(Request $request, Estudio $estudio)
    {
        $em = $this->getDoctrine()->getManager();
        $estudios = $em->getRepository('EstudiosIdiomasBundle:Estudio')->findAll();

        $form = $this->createForm(EstudioType::class, $estudio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                            'message' => 'The study has been modified correctly.'
                    )
                );

                return $this->redirectToRoute('estudio_new');
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'There is already a study "' . $form->get('estudio')->getData() . '" almacenado.';

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );

                $form->get('estudio')->addError(new FormError($message));
            } catch (PDOException $exception) {
                $message = 'There was an error modifying the study. ' . $exception->getMessage();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger',
                        'message' => $message
                    )
                );
                $form->get('estudio')->addError(new FormError($message));
            } catch (DBALException $exception) {
                $message = 'There was an error modifying the study. ' . $exception->getMessage();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger',
                        'message' => $message
                    )
                );
                $form->get('estudio')->addError(new FormError($message));
            }
        }

        return $this->render('@EstudiosIdiomas/estudio/new.html.twig', array(
            'estudio' => $estudio,
            'estudios' => $estudios,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Edit'
        ));
    }

    /**
     * Render template to confirm delete a estudio entity.
     *
     */
    public function deleteConfirmAction(Request $request, Estudio $estudio)
    {
        return $this->render('@EstudiosIdiomas/estudio/delete.html.twig', array(
            'estudio' => $estudio,
        ));
    }

    public function deleteAction(Request $request, Estudio $estudio)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($estudio);
            $em->flush($estudio);
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'The study has been successfully eliminated.'
                )
            );
        } catch (PDOException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger',
                    'message' => $message
                )
            );
        } catch (DBALException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger',
                    'message' => $message
                )
            );
        }

        return $this->redirectToRoute('estudio_new');
    }
}
