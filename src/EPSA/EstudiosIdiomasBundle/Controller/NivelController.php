<?php
/**
 * Created by PhpStorm.
 * User: fermi
 * Date: 19/05/2017
 * Time: 21:45
 */

namespace EPSA\EstudiosIdiomasBundle\Controller;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use EPSA\EstudiosIdiomasBundle\Entity\Nivel;
use EPSA\EstudiosIdiomasBundle\Form\NivelType;

/**
 * Nivel controller.
 *
 */
class NivelController extends Controller
{
    /**
     * Creates a new nivel entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $nivel = new Nivel();
        $nivel_list = $em->getRepository('EstudiosIdiomasBundle:Nivel')->findAll();

        $form = $this->createForm(NivelType::class, $nivel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($nivel);
                $em->flush($nivel);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The new level has been created correctly.'
                    )
                );
                return $this->redirectToRoute('nivel_new');

            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'There is already a level "' . $form->get('nivel')->getData() . '" almacenado.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );
                $message = 'There is already a level "' . $form->get('nivel')->getData() . '" almacenado.';
                $form->get('nivel')->addError(new FormError($message));
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
                $form->get('nivel')->addError(new FormError($message));
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
                $form->get('nivel')->addError(new FormError($message));
            }
        }

        return $this->render('@EstudiosIdiomas/nivel/new.html.twig', array(
            'nivel' => $nivel,
            'nivel_list' => $nivel_list,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'Nuevo',
        ));
    }

    /**
     * Displays a form to edit an existing nivel entity.
     *
     */
    public function editAction(Request $request, Nivel $nivel)
    {
        $em = $this->getDoctrine()->getManager();
        $nivel_list = $em->getRepository('EstudiosIdiomasBundle:Nivel')->findAll();

        $form = $this->createForm(NivelType::class, $nivel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The level has been modified correctly.'
                    )
                );

                return $this->redirectToRoute('nivel_new');
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'There is already a level "' . $form->get('nivel')->getData() . '" almacenado.';

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );

                $form->get('nivel')->addError(new FormError($message));
            } catch (PDOException $exception) {
                $message = 'An error occurred while modifying the level. ' . $exception->getMessage();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
                $form->get('nivel')->addError(new FormError($message));
            } catch (DBALException $exception) {
                $message = 'An error occurred while modifying the level. ' . $exception->getMessage();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
                $form->get('nivel')->addError(new FormError($message));
            }
        }

        return $this->render('@EstudiosIdiomas/nivel/new.html.twig', array(
            'nivel' => $nivel,
            'nivel_list' => $nivel_list,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Edit',
        ));
    }

    /**
     * Render template to confirm delete a nivel entity.
     *
     */
    public function deleteConfirmAction(Request $request, Nivel $nivel)
    {
        return $this->render('EstudiosIdiomasBundle:nivel:delete.html.twig', array(
            'nivel' => $nivel,
        ));
    }

    public function deleteAction(Nivel $nivel)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($nivel);
            $em->flush($nivel);
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'The level has been successfully deleted.'
                )
            );
        } catch (PDOException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        } catch (DBALException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        }

        return $this->redirectToRoute('nivel_new');
    }
}