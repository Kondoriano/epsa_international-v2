<?php

namespace EPSA\UniversidadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use EPSA\UniversidadesBundle\Entity\Universidad;
use EPSA\UniversidadesBundle\Entity\Pais;

class UniversidadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo', TextType::class, array(
                'label' => 'Code',
                'attr' => array(
                    'placeholder' => 'Ej: SF LATHI'
                ),
                'required' => true
            ))
            ->add('nombre', TextType::class, array(
                'label' => 'Name',
                'required' => true
            ))
            ->add('web', UrlType::class, array(
                'attr' => array(
                    'placeholder' => 'Ej: http://',
                ),
                'required' => false
            ))
            ->add('direccion', TextareaType::class, array(
                'label' => 'Address',
                'attr' => array(
                    'style' => 'resize:none',
                    'rows' => 2
                ),
                'required' => false
            ))
            ->add('coordenadas', TextType::class, array(
                'label' => 'Coordinates',
                'attr' => array(
                    'placeholder' => 'Ej: 0,987788988 - 9,22222'
                ),
                'required' => false
            ))
            ->add('plazas', IntegerType::class, array(
                'label' => 'Seats',
                'attr' => array(
                    'min' => 0
                ),
                'required' => false
            ))
            ->add('deadlineA', DateType::class, array(
                'label' => 'Dead line A',
                'widget' => 'single_text',
                'html5' => 'true',
                'required' => false
            ))
            ->add('deadlineB', DateType::class, array(
                'label' => 'Dead line B',
                'widget' => 'single_text',
                'html5' => 'true',
                'required' => false
            ))
            ->add('estado', CheckboxType::class, array(
                'label' => 'Status',
                'attr' => array(
                    'class' => 'flat',
                ),
                'label' => 'Visible / Oculto',
                'required' => false,
            ))
            ->add('pais', EntityType::class, array(
                'label' => 'Country',
                'class' => Pais::class,
                'choice_label' => 'nombre',
                'placeholder' => 'Select an option',
                'required' => false
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Universidad::class,
            'error_bubbling' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'epsa_universidadesbundle_universidad';
    }


}
