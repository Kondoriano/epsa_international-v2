<?php

namespace EPSA\ConvocatoriasBundle\Controller;

use EPSA\ConvocatoriasBundle\Entity\Convocatoria;
use EPSA\ConvocatoriasBundle\Form\ConvocatoriaType;
use EPSA\UniversidadesBundle\Entity\Universidad;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Convocatorium controller.
 *
 */
class ConvocatoriaController extends Controller
{
    /**
     * Lists all convocatorium entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $convocatorias = $em->getRepository('ConvocatoriasBundle:Convocatoria')->findAll();

        return $this->render('@Convocatorias/convocatoria/index.html.twig', array(
            'convocatorias' => $convocatorias,
        ));
    }

    /**
     * Creates a new convocatorium entity.
     *
     */
    public function newAction(Request $request)
    {
        // Referencia a repositorio universidades
        $em = $this->getDoctrine()->getManager();
        $repoUniversidad = $em->getRepository(Universidad::class);
        $universidades = count($repoUniversidad->findByEstado(true));
        $plazasConvocadas = $repoUniversidad->plazasConvocadas(true);

        $convocatoria = new Convocatoria();
        $form = $this->createForm(ConvocatoriaType::class, $convocatoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                // Añadimos datos a la Convocatoria: totalUniversidades, plazasOcupadas, plazasConvocadas
                $convocatoria->setTotalUniversidades($universidades);
                $convocatoria->setPlazasConvocadas($plazasConvocadas);
                $em->persist($convocatoria);
                $em->flush($convocatoria);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The call has been successfully created.'
                    )
                );

                return $this->redirectToRoute('convocatoria_index');
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'A stored call already exists.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            }
        }

        return $this->render('@Convocatorias/convocatoria/new.html.twig', array(
            'convocatoria' => $convocatoria,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'Nuevo'
        ));
    }

    /**
     * Displays a form to edit an existing convocatorium entity.
     *
     */
    public function editAction(Request $request, Convocatoria $convocatoria)
    {
        $form = $this->createForm(ConvocatoriaType::class, $convocatoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The call has been modified correctly.'
                    )
                );

                return $this->redirectToRoute('convocatoria_index');
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'A stored call already exists.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            }
        }

        return $this->render('@Convocatorias/convocatoria/edit.html.twig', array(
            'convocatoria' => $convocatoria,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Editar'
        ));
    }

    /**
     * Render a template to confirm delete a universidad entity.
     *
     */
    public function deleteConfirmAction(Request $request, Convocatoria $convocatoria)
    {
        return $this->render('@Convocatorias/convocatoria/delete.html.twig', array(
            'convocatoria' => $convocatoria,
        ));
    }

    public function deleteAction(Convocatoria $convocatorium)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($convocatorium);
            $em->flush($convocatorium);
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'The call has been successfully deleted.'
                )
            );
        } catch (PDOException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        } catch (DBALException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        }

        return $this->redirectToRoute('convocatoria_index');
    }
}
