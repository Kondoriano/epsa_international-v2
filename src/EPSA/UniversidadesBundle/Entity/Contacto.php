<?php

namespace EPSA\UniversidadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint as UNICIDAD;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contacto
 *
 * @ORM\Table(
 *     name="contacto",
 *     uniqueConstraints={@UNICIDAD(name="unicidad", columns={"nombre", "email", "cargo_id", "universidad_id"})}
 * )
 * @ORM\Entity(repositoryClass="EPSA\UniversidadesBundle\Repository\ContactoRepository")
 */
class Contacto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="text", nullable=true)
     */
    private $direccion;

    /**
     * @var Cargo
     *
     * @ORM\ManyToOne(targetEntity="Cargo")
     * @Assert\NotBlank()
     */
    private $cargo;

    /**
     * @var Universidad
     *
     * @ORM\ManyToOne(targetEntity="Universidad", inversedBy="contactos")
     * @ORM\JoinColumn(name="universidad_id", referencedColumnName="id")
     */
    private $universidad;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Contacto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param string $apellidos
     * @return Contacto
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contacto
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Contacto
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Contacto
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @return Cargo
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * @param Cargo $cargo
     * @return Contacto
     */
    public function setCargo(Cargo $cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * @return Universidad
     */
    public function getUniversidad()
    {
        return $this->universidad;
    }

    /**
     * @param Universidad $universidad
     * @return Contacto
     */
    public function setUniversidad(Universidad $universidad)
    {
        $this->universidad = $universidad;

        return $this;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}

