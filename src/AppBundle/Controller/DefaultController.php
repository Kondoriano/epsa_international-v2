<?php
/**
 * Created by Fermín Martínez Gombao.
 * User: fermi
 * Date: 18/02/2017
 * Time: 20:30
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Controlador de la aplicación base que unicamente incluye la HomePage.
     *
     * @Route("/{_locale}/", name="homepage", requirements={"_locale" = "fr|en|es|de"})
     * @Route("/", name="homepage_locale", defaults={"_locale" = "en"},)
     */
    public function indexAction(Request $request)
    {
        /* If user is admin, redirect to dashboard */
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('dashboard_dashboard');
        }else{
            return $this->render('default/index.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            ]);
        }

    }

    /**
     * Controlador About us
     *
     * @Route("/about", name="about_us")
     */
    public function AboutAction(Request $request)
    {
        return $this->render('default/about.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }
}
