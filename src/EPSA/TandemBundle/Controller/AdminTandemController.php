<?php

namespace EPSA\TandemBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EPSA\TandemBundle\Entity\Tandem;

class AdminTandemController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tandems = $em->getRepository('TandemBundle:Tandem')->findAll();

        return $this->render('TandemBundle:admin_tandem:index.html.twig', array(
            'tandems' => $tandems,
        ));
    }


    /**
     * Finds and displays a Tandem entity.
     *
     */
    public function showAction(Tandem $tandem)
    {
        $deleteForm = $this->createDeleteForm($tandem);
        $approveStatusForm = $this->createApproveStatusForm($tandem);
        $denyStatusForm = $this->createDenyStatusForm($tandem);
        $form = $this->createForm('EPSA\TandemBundle\Form\TandemType', $tandem);

        return $this->render('TandemBundle:admin_tandem:show.html.twig', array(
            'tandem' => $tandem,
            'delete_form' => $deleteForm->createView(),
            'approveStatus_form' => $approveStatusForm->createView(),
            'denyStatus_form' => $denyStatusForm->createView(),
            'form' => $form->createView(),
        ));
    }


    /**
     * Deletes a Tandem entity.
     *
     */
    public function deleteConfirmAction(Tandem $tandem)
    {
        if ($this->getUser() == $tandem->getUsuario()  or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {
            return $this->render('TandemBundle:admin_tandem:delete.html.twig', array(
                'tandem' => $tandem,
            ));
        }
        return $this->redirectToRoute('admin_tandem_index');
    }


    public function deleteAction(Request $request, Tandem $tandem)
    {
        if ($this->getUser() == $tandem->getUsuario() or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {
            $form = $this->createDeleteForm($tandem);
            $form->handleRequest($request);

            $em = $this->getDoctrine()->getManager();
            $em->remove($tandem);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Congratulations!',
                    'message' => 'Your Tandem has been deleted!'
                )
            );

        }
        return $this->redirectToRoute('admin_tandem_index');

    }

    /**
     * Creates a form to delete a Tandem entity.
     *
     * @param Tandem $tandem The Tandem entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tandem $tandem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tandem_delete', array('id' => $tandem->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    //APROBAR TANDEM
    public function approveStatusAction(Request $request, Tandem $tandem)
    {
        $form = $this->createApproveStatusForm($tandem);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $tandem->setEstadoTandem("Approved");

        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'notice',
            array(
                'alert' => 'success',
                'title' => 'Good job,',
                'message' => 'Tandem has been approved.'
            )
        );
        return $this->redirectToRoute('admin_tandem_index');
    }

    private function createApproveStatusForm(Tandem $tandem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_approveStatus', array('id' => $tandem->getId())))
            ->getForm();
    }

    //RECHAZAR TANDEM
    public function denyStatusAction(Request $request, Tandem $tandem)
    {
        $form = $this->createDenyStatusForm($tandem);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $tandem->setEstadoTandem("Rejected");

        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'notice',
            array(
                'alert' => 'success',
                'title' => 'Good job,',
                'message' => 'Tandem has been rejected.'
            )
        );
        return $this->redirectToRoute('admin_tandem_index');
    }

    private function createDenyStatusForm(Tandem $tandem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_denyStatus', array('id' => $tandem->getId())))
            ->getForm();
    }

}
