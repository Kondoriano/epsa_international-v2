<?php

namespace EPSA\ExperienciasBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use EPSA\ExperienciasBundle\Entity\Experiencia;
use EPSA\ExperienciasBundle\Form\AdminExperienciaType;

/**
 * Experiencia controller.
 *
 */
class AdminExperienciaController extends Controller
{
    /**
     * Lists all experiencia entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $experiencias = $em->getRepository('ExperienciasBundle:Experiencia')->findAll();

        return $this->render('@Experiencias/admin_experiencia/index.html.twig', array(
            'experiencias' => $experiencias
        ));
    }

    /**
     * Displays a form to edit an existing experiencium entity.
     *
     */
    public function editAction(Request $request, Experiencia $experiencia)
    {
        $form = $this->createForm(AdminExperienciaType::class, $experiencia);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form['estado']->getData();

            $em = $this->getDoctrine()->getManager();
            $experiencia->setEstado($data);
            $experiencia->setNueva(false);
            $em->persist($experiencia);
            $em->flush($experiencia);

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'The experience has been modified correctly.'
                )
            );

            return $this->redirectToRoute('admin_experiencia_index');
        }

        return $this->render('@Experiencias/admin_experiencia/edit.html.twig', array(
            'experiencia' => $experiencia,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Edit'
        ));

    }

}
