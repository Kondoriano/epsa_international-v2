<?php
/**
 * Created by PhpStorm.
 * User: fermi
 * Date: 19/05/2017
 * Time: 21:46
 */

namespace EPSA\EstudiosIdiomasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use EPSA\EstudiosIdiomasBundle\Entity\Nivel;


class NivelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nivel', TextType::class, array(
                'label' => 'Level',
                'required' => false
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Nivel::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'epsa_estudiosidiomasbundle_idioma';
    }


}