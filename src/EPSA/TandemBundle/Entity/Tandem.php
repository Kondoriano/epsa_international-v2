<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 24/09/2016
 * Time: 20:42
 */

namespace EPSA\TandemBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tandem")
 * @ORM\Entity
 */

class Tandem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /** @ORM\Column(type="date") */
    protected $fecha_tandem;

    /** @ORM\Column(type="time") */
    protected $hora_tandem;

    /** @ORM\Column(type="string", length=100) */
    protected $lugar_tandem;

    /** @ORM\Column(type="string", length=40) */
    protected $idioma_tandem_ensenyar;

    /** @ORM\Column(type="string", length=40) */
    protected $idioma_tandem_aprender;

    /** @ORM\Column(type="string") */
    protected $estudio;

    /** @ORM\Column(type="integer") */
    protected $max_personas;

    /** @ORM\Column(type="integer") */
    protected $personas_apuntadas = 1;

    /** @ORM\Column (type="string", length=20)  */
    protected $estadoTandem = "Pending";

    //Relación N:1 entre la tabla Tandem y Usuario
    //Clave ajena para guardar al creador del tandem
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario", inversedBy="tandems")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    //Relación N:M entre la tabla Tandem y Usuario
    //Clave ajena para guardar participantes en un tandem
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Usuario", inversedBy="usuarios")
     * @ORM\JoinTable(name="tandem_participaciones")
     */
    private $participantes;


        //Getters & Setters

    public function __construct(){
        $this->fecha_tandem = new \DateTime();
        $this->participantes = new ArrayCollection();
    }

    public function __toString()
    {
        return (String)$this->getId();
    }

    /**
     * Get tandem
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getEstadoTandem()
    {
        return $this->estadoTandem;
    }

    /**
     * @param mixed $estadoTandem
     */
    public function setEstadoTandem($estadoTandem)
    {
        $this->estadoTandem = $estadoTandem;
    }


    /**
     * Set fechaTandem
     *
     * @param \DateTime $fechaTandem
     *
     * @return Tandem
     */
    public function setFechaTandem($fechaTandem)
    {
        $this->fecha_tandem = $fechaTandem;

        return $this;
    }

    /**
     * Get fechaTandem
     *
     * @return \DateTime
     */
    public function getFechaTandem()
    {
        return $this->fecha_tandem;
    }

    /**
     * Set lugarTandem
     *
     * @param string $lugarTandem
     *
     * @return Tandem
     */
    public function setLugarTandem($lugarTandem)
    {
        $this->lugar_tandem = $lugarTandem;

        return $this;
    }

    /**
     * Get lugarTandem
     *
     * @return string
     */
    public function getLugarTandem()
    {
        return $this->lugar_tandem;
    }


    /**
     * Set maxPersonas
     *
     * @param integer $maxPersonas
     *
     * @return Tandem
     */
    public function setMaxPersonas($maxPersonas)
    {
        $this->max_personas = $maxPersonas;

        return $this;
    }

    /**
     * Get maxPersonas
     *
     * @return integer
     */
    public function getMaxPersonas()
    {
        return $this->max_personas;
    }

    /**
     * Set personasApuntadas
     *
     * @param integer $personasApuntadas
     *
     * @return Tandem
     */
    public function setPersonasApuntadas($personasApuntadas)
    {
        $this->personas_apuntadas = $personasApuntadas;

        return $this;
    }

    /**
     * Get personasApuntadas
     *
     * @return integer
     */
    public function getPersonasApuntadas()
    {
        return $this->personas_apuntadas;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     *
     * @return Tandem
     */
    public function setUsuario( $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set horaTandem
     *
     * @param \DateTime $horaTandem
     *
     * @return Tandem
     */
    public function setHoraTandem($horaTandem)
    {
        $this->hora_tandem = $horaTandem;

        return $this;
    }

    /**
     * Get horaTandem
     *
     * @return \DateTime
     */
    public function getHoraTandem()
    {
        return $this->hora_tandem;
    }

    /**
     * Set idiomaTandemEnsenyar
     *
     * @param string $idiomaTandemEnsenyar
     *
     * @return Tandem
     */
    public function setIdiomaTandemEnsenyar($idiomaTandemEnsenyar)
    {
        $this->idioma_tandem_ensenyar = $idiomaTandemEnsenyar;

        return $this;
    }

    /**
     * Get idiomaTandemEnsenyar
     *
     * @return string
     */
    public function getIdiomaTandemEnsenyar()
    {
        return $this->idioma_tandem_ensenyar;
    }

    /**
     * Set idiomaTandemAprender
     *
     * @param string $idiomaTandemAprender
     *
     * @return Tandem
     */
    public function setIdiomaTandemAprender($idiomaTandemAprender)
    {
        $this->idioma_tandem_aprender = $idiomaTandemAprender;

        return $this;
    }

    /**
     * Get idiomaTandemAprender
     *
     * @return string
     */
    public function getIdiomaTandemAprender()
    {
        return $this->idioma_tandem_aprender;
    }

    /**
     * Add participante
     *
     * @param \AppBundle\Entity\Usuario $participante
     *
     * @return Tandem
     */
    public function addParticipante(\AppBundle\Entity\Usuario $participante)
    {
        $this->participantes[] = $participante;

        return $this;
    }

    /**
     * Remove participante
     *
     * @param \AppBundle\Entity\Usuario $participante
     */
    public function removeParticipante(\AppBundle\Entity\Usuario $participante)
    {
        $this->participantes->removeElement($participante);
    }

    /**
     * Get participantes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipantes()
    {
        return $this->participantes->toArray();
    }

    /**
     * @return mixed
     */
    public function getEstudio()
    {
        return $this->estudio;
    }

    /**
     * @param mixed $estudio
     */
    public function setEstudio($estudio)
    {
        $this->estudio = $estudio;
    }



}
