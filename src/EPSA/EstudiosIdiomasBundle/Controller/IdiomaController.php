<?php

namespace EPSA\EstudiosIdiomasBundle\Controller;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use EPSA\EstudiosIdiomasBundle\Entity\Idioma;
use EPSA\EstudiosIdiomasBundle\Form\IdiomaType;

/**
 * Idioma controller.
 *
 */
class IdiomaController extends Controller
{
    /**
     * Creates a new idioma entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idiomas = $em->getRepository('EstudiosIdiomasBundle:Idioma')->findAll();

        $idioma = new Idioma();
        $form = $this->createForm(IdiomaType::class, $idioma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($idioma);
                $em->flush($idioma);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The new language has been created correctly.'
                    )
                );
                return $this->redirectToRoute('idioma_new');

            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'Language already exists "' . $form->get('idioma')->getData() . '" almacenado.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );
                $message = 'Ya existe un idioma "' . $form->get('idioma')->getData() . '" almacenado.';
                $form->get('idioma')->addError(new FormError($message));
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
                $form->get('idioma')->addError(new FormError($message));
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Error!',
                        'message' => $message
                    )
                );
                $form->get('idioma')->addError(new FormError($message));
            }
        }

        return $this->render('@EstudiosIdiomas/idioma/new.html.twig', array(
            'idioma' => $idioma,
            'idiomas' => $idiomas,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'New',
        ));
    }

    /**
     * Displays a form to edit an existing idioma entity.
     *
     */
    public function editAction(Request $request, Idioma $idioma)
    {
        $em = $this->getDoctrine()->getManager();
        $idiomas = $em->getRepository('EstudiosIdiomasBundle:Idioma')->findAll();

        $form = $this->createForm(IdiomaType::class, $idioma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The language has been modified correctly.'
                    )
                );

                return $this->redirectToRoute('idioma_new');
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'Language already exists "' . $form->get('estudio')->getData() . '" almacenado.';

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );

                $form->get('idioma')->addError(new FormError($message));
            } catch (PDOException $exception) {
                $message = 'There was an error modifying the language. ' . $exception->getMessage();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
                $form->get('idioma')->addError(new FormError($message));
            } catch (DBALException $exception) {
                $message = 'There was an error modifying the language. ' . $exception->getMessage();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
                $form->get('idioma')->addError(new FormError($message));
            }
        }

        return $this->render('@EstudiosIdiomas/idioma/new.html.twig', array(
            'idioma' => $idioma,
            'idiomas' => $idiomas,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Edit',
        ));
    }

    /**
     * Render template to confirm delete a estudio entity.
     *
     */
    public function deleteConfirmAction(Request $request, Idioma $idioma)
    {
        return $this->render('EstudiosIdiomasBundle:idioma:delete.html.twig', array(
            'idioma' => $idioma,
        ));
    }

    public function deleteAction(Idioma $idioma)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($idioma);
            $em->flush($idioma);
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'The language has been deleted successfully.'
                )
            );
        } catch (PDOException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        } catch (DBALException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        }

        return $this->redirectToRoute('idioma_new');
    }
}
