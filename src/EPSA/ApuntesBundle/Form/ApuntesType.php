<?php

namespace EPSA\ApuntesBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApuntesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreArchivo', FileType::class, array(
                'label' => 'PDF file', 'data_class' => null,
                'attr' => array(
                    'oninvalid' => "setCustomValidity('Complete this field')",
                    'onchange' => "setCustomValidity('')"
                )
            ))
            ->add('idiomaArchivo', EntityType::class, array(
                'class' => 'EstudiosIdiomasBundle:Idioma',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->groupBy('i.idioma');
                },
                'label' => 'File language',
                'placeholder' => 'Choose a language',
                'attr' => array('oninvalid' => "setCustomValidity('You must choose a language!')",
                    'oninput' => "setCustomValidity('')")
            ))
            ->add('carreraArchivo', EntityType::class, array(
                'class' => 'EstudiosIdiomasBundle:Estudio',
                'choice_label' => function ($estudio) {
                    return $estudio->getEstudio();
                },
                'label' => 'Career name',
                'placeholder' => 'Choose your career',
                'attr' => array('oninvalid' => "setCustomValidity('You must choose a career!')",
                    'oninput' => "setCustomValidity('')")
            ))
            ->add('cursoArchivo', TextType::class, array(
                'label' => 'Course',
                'attr' => array(
                    'placeholder' => 'E.g : 1',
                    'oninvalid' => "setCustomValidity('Complete this field')",
                    'oninput' => "setCustomValidity('')"
                )
            ))
            ->add('cuatrimestreArchivo', ChoiceType::class, array(
                    'label' => 'Semester',
                    'choices' => array(
                        'A' => 'A',
                        'B' => 'B',
                        'A + B' => 'A + B'
                    ),
                    'placeholder' => 'Choose a semester',
                    'attr' => array(
                        'oninvalid' => "setCustomValidity('Complete this field')",
                        'oninput' => "setCustomValidity('')"
                    )
                )
            )
            ->add('asignaturaArchivo', TextType::class, array(
                'label' => 'Subject (name and code)',
                'attr' => array(
                    'placeholder' => 'E.g : Business Law (11817)',
                    'oninvalid' => "setCustomValidity('Complete this field')",
                    'oninput' => "setCustomValidity('')"
                )
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EPSA\ApuntesBundle\Entity\Apuntes'
        ));
    }
}
