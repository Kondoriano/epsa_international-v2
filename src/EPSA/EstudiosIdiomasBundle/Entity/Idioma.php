<?php

namespace EPSA\EstudiosIdiomasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint as UNICIDAD;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Idioma
 *
 * @ORM\Table(
 *     name="idioma",
 * )
 * @ORM\Entity(repositoryClass="EPSA\EstudiosIdiomasBundle\Repository\IdiomaRepository")
 */
class Idioma
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $idioma;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idioma
     *
     * @param string $idioma
     *
     * @return Idioma
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    public function __toString()
    {
        return $this->getIdioma();
    }
}

