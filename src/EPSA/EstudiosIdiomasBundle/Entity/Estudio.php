<?php

namespace EPSA\EstudiosIdiomasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Estudio
 *
 * @ORM\Table(name="estudio")
 * @ORM\Entity(repositoryClass="EPSA\EstudiosIdiomasBundle\Repository\EstudioRepository")
 */
class Estudio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="estudio", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $estudio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estudio
     *
     * @param string $estudio
     *
     * @return Estudio
     */
    public function setEstudio($estudio)
    {
        $this->estudio = $estudio;

        return $this;
    }

    /**
     * Get estudio
     *
     * @return string
     */
    public function getEstudio()
    {
        return $this->estudio;
    }

    function __toString()
    {
        return $this->getEstudio();
    }


}

