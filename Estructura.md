# Frontend
Parte pública de la web, donde todo usuario sin registrar ni logeado podrá acceder y ver la información disponible para
su rol (anonymus). A continuación se exponen las páginas disponibles:
- Portada: Presentación del sitio por David Gutierrez y listado de Apps diponibles.
- Listado de Tandems: unicamente se presentarán el listado de tandems. Se habilitarán opciones en función del rol del
usuario logeado.
- Destinos Erasmus: Mapa con filtro de los Destinos donde se puede optar.
- Destinos Erasmus - Detalle: detalle de la universidad seleccionada en el mapa.
- Experiencias Erasmus: unicamente se presentará el listado de Experiencias ya publicadas y aprobadas. Se habilitarán
opciones en función del rol del usuario logeado.
- Apuntes: ¿?

# Intranet
Parte privada de los usuarios registrados.
- Perfil:
- Tandems: listado de tandem creados por el usuario (crear, editar, eliminar).
- Experiencias: listado de experiencias creadas por el usuario (crear, editar, eliminar).
- Apuntes: ¿?

# Backend
Parte privada, reservada unicamente a los administradores (técnicos) de la web.
- Universidades: CRUD
- Contactos: CRUD
- Estudios/Idiomas: CRUD
- Convocatorias: información de las convocatorias publicadas con gráficos y estadísticas.
- Tandems: listado de todos los Tandem publicados por los usuarios. UNICAMENTE SE PODRÁ PUBLICAR O RECHAZAR el Tandem,
en ningún momento se podrán modificar datos introducidos por el Usuario.
- Experiencias: listado de todas las Experiencias publicadas por los usuarios. UNICAMENTE SE PODRÁ PUBLICAR O RECHAZAR
la Experiencia, en ningún momento se podrán modificar los datos introducidos por el Usuario.
- Usuarios: 