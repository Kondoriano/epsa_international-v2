<?php
/**
 * Created by PhpStorm.
 * User: fermi
 * Date: 20/05/2017
 * Time: 12:45
 */

namespace EPSA\UniversidadesBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

use EPSA\UniversidadesBundle\Form\MailingType;

class MailingController extends Controller
{
    /**
     * Lists all contacto entities.
     *
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(MailingType::class);
        $form->handleRequest($request);
        $contactos = "hola";

        if ($form->isSubmitted() && $form->isValid()) {
            # Cuando se recibe el formulario se buscan contactos según el criterio de búsqueda
            # Preparamos el manager
            $em = $this->getDoctrine()->getManager();

            # Obtenemos el valor de position para filtrar los contactos
            $position = $form['position']->getData();
            if ($position != ""){
                $contactos = $em->getRepository('UniversidadesBundle:Contacto')->findBy(
                    array('cargo' => $position)
                );
            }else{
                $contactos = $em->getRepository('UniversidadesBundle:Contacto')->findAll();
            }
            $content = $form['content']->getData();

            $mail_contactos = [];
            foreach ($contactos as $contacto){
                array_push($mail_contactos, $contacto->getEmail());
            }

            $message = \Swift_Message::newInstance()
                ->setSubject('Some Subject')
                ->setFrom('fermin.margo@gmail.com')
                ->setTo($mail_contactos)
                ->setBody(
                    $this->renderView(
                        '@Universidades/mailing/mail_template.html.twig',
                        array('body' => $content)
                    ),
                    'text/html'
                );
            try{
                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The message has been sent correctly.'
                    )
                );
            } catch (Exception $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            }

        }

        return $this->render('@Universidades/mailing/send.html.twig', array(
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'New',
        ));
    }
}