<?php

namespace EPSA\ApuntesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Apuntes
 *
 * @ORM\Table(name="apuntes")
 * @ORM\Entity(repositoryClass="EPSA\ApuntesBundle\Repository\ApuntesRepository")
 */
class Apuntes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(
     *     mimeTypes={ "application/pdf" },
     *     mimeTypesMessage = "The file must be a PDF",
     *     maxSize = "5120k",
     *     maxSizeMessage = "The maximum size for a file is 5MB")
     */
    protected $nombreArchivo;

    /**
     * @ORM\Column(type="string")
     */
    protected $idiomaArchivo;

    /**
     * @ORM\Column(type="string")
     */
    protected $carreraArchivo;

    /**
     * @ORM\Column(type="integer")
     */
    protected $cursoArchivo;

    /**
     * @ORM\Column(type="string")
     */
    protected $cuatrimestreArchivo;

    /**
     * @ORM\Column(type="string")
     */
    protected $asignaturaArchivo;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fechaSubida;

    /**
     * @ORM\Column(type="string")
     */
    protected $nombreOriginal;

    //Relación N:1 entre la tabla Apuntes y Usuario
    //Clave ajena para guardar al creador del archivo
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario", inversedBy="archivos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;



    public function __toString(){
        return (String)$this->getId();
    }

    public function __construct()
    {
        $this->fechaSubida = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombreArchivo()
    {
        return $this->nombreArchivo;
    }

    /**
     * @param mixed $nombreArchivo
     */
    public function setNombreArchivo($nombreArchivo)
    {
        $this->nombreArchivo = $nombreArchivo;
    }

    /**
     * @return mixed
     */
    public function getIdiomaArchivo()
    {
        return $this->idiomaArchivo;
    }

    /**
     * @param mixed $idiomaArchivo
     */
    public function setIdiomaArchivo($idiomaArchivo)
    {
        $this->idiomaArchivo = $idiomaArchivo;
    }

    /**
     * @return mixed
     */
    public function getCarreraArchivo()
    {
        return $this->carreraArchivo;
    }

    /**
     * @param mixed $carreraArchivo
     */
    public function setCarreraArchivo($carreraArchivo)
    {
        $this->carreraArchivo = $carreraArchivo;
    }

    /**
     * @return mixed
     */
    public function getCursoArchivo()
    {
        return $this->cursoArchivo;
    }

    /**
     * @param mixed $cursoArchivo
     */
    public function setCursoArchivo($cursoArchivo)
    {
        $this->cursoArchivo = $cursoArchivo;
    }

    /**
     * @return mixed
     */
    public function getCuatrimestreArchivo()
    {
        return $this->cuatrimestreArchivo;
    }

    /**
     * @param mixed $cuatrimestreArchivo
     */
    public function setCuatrimestreArchivo($cuatrimestreArchivo)
    {
        $this->cuatrimestreArchivo = $cuatrimestreArchivo;
    }

    /**
     * @return mixed
     */
    public function getAsignaturaArchivo()
    {
        return $this->asignaturaArchivo;
    }

    /**
     * @param mixed $asignaturaArchivo
     */
    public function setAsignaturaArchivo($asignaturaArchivo)
    {
        $this->asignaturaArchivo = $asignaturaArchivo;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return \DateTime
     */
    public function getFechaSubida()
    {
        return $this->fechaSubida;
    }

    /**
     * @param mixed $fechaSubida
     */
    public function setFechaSubida($fechaSubida)
    {
        $this->fechaSubida = $fechaSubida;
    }

    /**
     * @return mixed
     */
    public function getNombreOriginal()
    {
        return $this->nombreOriginal;
    }

    /**
     * @param mixed $nombreOriginal
     */
    public function setNombreOriginal($nombreOriginal)
    {
        $this->nombreOriginal = $nombreOriginal;
    }

    public function getFile(){
        return $this->file;
    }
}

