<?php
/**
 * Created by Fermín Martínez Gombao.
 * User: fermi
 * Date: 18/02/2017
 * Time: 20:30
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;


/**
 * Class RegistrationType.
 * Formulario para el registro de nuevos usuarios en la plataforma.
 * @package AppBundle\Form
 */
class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Name',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => true
            ))
            ->add('apellido', TextType::class, array(
                'label' => 'Surname',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => true
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Telephone',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => true
            ))
            ->add('sexo', ChoiceType::class, array(
                'label' => 'Sex',
                'choices' => array(
                    'Male' => 'male',
                    'Female' => 'female',
                ),
                'required' => true
            ))
            ->add('procedencia_usuario', ChoiceType::class, array(
                'label' => 'User provenance',
                'choices' => array(
                    'Erasmus' => 'erasmus',
                    'Student' => 'student',
                ),
                'required' => true
            ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'usuario_bundle_registration_type';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
