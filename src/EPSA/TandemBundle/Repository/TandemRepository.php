<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 04/01/2017
 * Time: 14:40
 */

namespace EPSA\TandemBundle\Entity;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class TandemRepository extends EntityRepository{
    public function findTandemById($id){
        $em = $this->getEntityManager();
        $query = $em->createQuery('
            SELECT t, u
            FROM TandemBundle:Tandem t, UsuarioBundle:Usuario u
            JOIN t.usuario u
            WHERE t.id = :id
        ');
        $query->setParameter('id', $id);

        try {
            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}