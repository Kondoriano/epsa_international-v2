<?php
/**
 * Created by PhpStorm.
 * User: Fermín Martínez Gombao
 * Date: 18/01/2017
 * Time: 23:03
 */

namespace EPSA\DestinosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use EPSA\DestinosBundle\Form\DestinosType;
use EPSA\UniversidadesBundle\Entity\Universidad;

class DestinoController extends Controller
{
    /**
     * Función para mostrar unicamente el Template y cargar el formulario para el filtrado de opciones en la búsqueda
     * de Destinos.
     * @return Response
     */
    public function indexAction()
    {
        $form = $this->createForm(DestinosType::class);

        return $this->render('@Destinos/destinos/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Función para mostrar el detalle de una Universidad.
     */
    public function showAction(Universidad $universidad){
        return $this->render('@Destinos/destinos/show.html.twig', array(
            'universidad' => $universidad,
        ));
    }

    /**
     * Función que devuelve en formato JSON información: {id - coordenadas} de todas las universidades.
     * Recibe por parámetros GET los datos: pais_id, estudio_id, idioma_id y llamando al repositorio de
     * Universidades -> filterCoordenadas, devuelve la información.
     * @param Request $request
     * @return Response en formato JSON
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $universidades = $em->getRepository('UniversidadesBundle:Universidad')->filterCoordenadas(
            $request->query->get('pais'), $request->query->get('estudio'), $request->query->get('idioma')
        );

        $response = new Response(json_encode($universidades));

        return $response;
    }
}