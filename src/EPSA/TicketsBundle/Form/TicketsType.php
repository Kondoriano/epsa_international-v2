<?php

namespace EPSA\TicketsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', TextType::class, array(
                'label' => 'Title',
                'attr' => array(
                    'placeholder' => 'Write the title of your issue',
                    'requiered' => true,
                    'oninvalid' => "setCustomValidity('You must enter a title for your issue')",
                    'oninput' => "setCustomValidity('')"
                )
            ))
            ->add('apartado', ChoiceType::class, array(
                'label' => 'Section',
                'choices' => array(
                    'Experiences' => 'Experiences',
                    'Destinations' => 'Destinations',
                    'Tandems' => 'Tandems',
                    'Shared resources' => 'Shared resources',
                    'User control panel' => 'User control panel',
                    'Others' => 'Others'
                ),
                'placeholder' => 'Choose the section of the issue',
                'attr' => array(
                    'oninvalid' => "setCustomValidity('You must choose a category')",
                    'oninput' => "setCustomValidity('')"
                )
            ))
            //->add('estado_ticket')
            ->add('descripcion', TextareaType::class, array(
                'label' => 'Description',
                'attr' => array(
                    'placeholder' => 'Describe the issue',
                    'requiered' => true,
                    'oninvalid' => "setCustomValidity('You must write a description of your problem')",
                    'oninput' => "setCustomValidity('')"
                )
            ))
            //->add('fecha_creacion', 'datetime')
            //->add('fecha_resolucion', 'datetime')
            //->add('usuario_resolucion')
            //->add('usuario')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EPSA\TicketsBundle\Entity\Tickets'
        ));
    }
}
