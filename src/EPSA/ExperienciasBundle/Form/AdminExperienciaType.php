<?php
/**
 * Created by PhpStorm.
 * User: fermi
 * Date: 04/03/2017
 * Time: 21:07
 */

namespace EPSA\ExperienciasBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminExperienciaType extends AbstractType
{
    /**
     * Método que construye el formulario. Se definen los campos que tendrá.
     * Nótese como se hace uso de EntityType, para hacer referencia a que será un campo referente a una Entidad.
     * choice_label -> indica el texto que se mostrará en el desplegable, como se puede ver en el Idioma, se ha definido
     * una función para que muestre la combinación de idioma + nivel. Esto se hace porque por defecto en la variable
     * choice_label solo se puede asociar atributos de la entidad (idioma, nivel) de forma individual.
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estado', CheckboxType::class, array(
                'label' => 'Currente state',
                'required' => false
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EPSA\ExperienciasBundle\Entity\Experiencia'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'epsa_experienciasbundle_experiencia';
    }
}