<?php

namespace EPSA\TandemBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TandemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha_tandem', DateType::class, array(
                //'years' => range(date('Y')+2, date('Y'))
                'widget' => 'single_text',
                'html5' => 'true',
                'label' => 'Tandem date'
            ))
            ->add('hora_tandem', TimeType::class, array(
                'label' => 'Tandem time'
            ))
            ->add('lugar_tandem', TextType::class, array(
                'label' => 'Tandem place',
                'attr' => array('oninvalid' => "setCustomValidity('You must enter a place')",
                    'oninput' => "setCustomValidity('')")
                ))
            ->add('estudio', EntityType::class, array(
                'class' => 'EstudiosIdiomasBundle:Estudio',
                'choice_label' => function ($estudio) {
                    return $estudio->getEstudio();
                },
                'label' => 'Career',
                'placeholder' => 'Choose your career',
                'attr' => array('oninvalid' => "setCustomValidity('You must choose a career!')",
                    'oninput' => "setCustomValidity('')")
            ))
            ->add('idioma_tandem_ensenyar', EntityType::class, array(
                'class' => 'EstudiosIdiomasBundle:Idioma',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->groupBy('i.idioma');
                },
                'label' => 'Language you want to learn',
                'placeholder' => 'Choose an option',
                'attr' => array('oninvalid' => "setCustomValidity('You must choose a language!')",
                    'oninput' => "setCustomValidity('')")
            ))
            ->add('idioma_tandem_aprender', EntityType::class, array(
                'class' => 'EstudiosIdiomasBundle:Idioma',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->groupBy('i.idioma');
                },
                'label' => 'Language you want to teach',
                'placeholder' => 'Choose an option',
                'attr' => array('oninvalid' => "setCustomValidity('You must choose a language!')",
                    'oninput' => "setCustomValidity('')")
            ))
            ->add('max_personas', IntegerType::class, array(
                'label' => 'Maximum amount of participants'
            ))
            //->add('personas_apuntadas')
            //->add('usuario')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EPSA\TandemBundle\Entity\Tandem'
        ));
    }
}
