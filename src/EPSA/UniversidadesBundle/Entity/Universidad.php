<?php

namespace EPSA\UniversidadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint as UNICIDAD;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Universidad
 *
 * @ORM\Table(
 *     name="universidad",
 *     uniqueConstraints={@UNICIDAD(name="unicidad", columns={"codigo"})}
 * )
 * @ORM\Entity(repositoryClass="EPSA\UniversidadesBundle\Repository\UniversidadRepository")
 * @UniqueEntity("codigo")
 *
 */
class Universidad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=20, unique=true)
     * @Assert\NotBlank()
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    private $web;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="text", nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="coordenadas", type="string", length=255, nullable=true)
     */
    private $coordenadas;

    /**
     * @var int
     *
     * @ORM\Column(name="plazas", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(
     *      value=0
     * )
     */
    private $plazas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadlineA", type="date", nullable=true)
     * @Assert\Date()
     */
    private $deadlineA;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadlineB", type="date", nullable=true)
     * @Assert\Date()
     */
    private $deadlineB;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     * @Assert\Type(type="bool")
     */
    private $estado;

    /**
     * @var Pais
     *
     * @ORM\ManyToOne(targetEntity="Pais")
     * @Assert\NotBlank()
     */
    private $pais;

    /**
     * @var Contacto[]
     *
     * @ORM\OneToMany(targetEntity="Contacto", mappedBy="universidad")
     */
    private $contactos;

    /**
     * @var OfertaAcademica
     *
     * @ORM\OneToMany(targetEntity="OfertaAcademica", mappedBy="universidad")
     */
    private $ofertasAcademicas;

    public function __construct()
    {
        $this->contactos = new ArrayCollection();
        $this->ofertasAcademicas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Universidad
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Universidad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set web
     *
     * @param string $web
     *
     * @return Universidad
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Universidad
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set coordenadas
     *
     * @param string $coordenadas
     *
     * @return Universidad
     */
    public function setCoordenadas($coordenadas)
    {
        $this->coordenadas = $coordenadas;

        return $this;
    }

    /**
     * Get coordenadas
     *
     * @return string
     */
    public function getCoordenadas()
    {
        return $this->coordenadas;
    }

    /**
     * Set plazas
     *
     * @param integer $plazas
     *
     * @return Universidad
     */
    public function setPlazas($plazas)
    {
        $this->plazas = $plazas;

        return $this;
    }

    /**
     * Get plazas
     *
     * @return int
     */
    public function getPlazas()
    {
        return $this->plazas;
    }

    /**
     * Set deadlineA
     *
     * @param \DateTime $deadlineA
     *
     * @return Universidad
     */
    public function setDeadlineA($deadlineA)
    {
        $this->deadlineA = $deadlineA;

        return $this;
    }

    /**
     * Get deadlineA
     *
     * @return \DateTime
     */
    public function getDeadlineA()
    {
        return $this->deadlineA;
    }

    /**
     * Set deadlineB
     *
     * @param \DateTime $deadlineB
     *
     * @return Universidad
     */
    public function setDeadlineB($deadlineB)
    {
        $this->deadlineB = $deadlineB;

        return $this;
    }

    /**
     * Get deadlineB
     *
     * @return \DateTime
     */
    public function getDeadlineB()
    {
        return $this->deadlineB;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Universidad
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return bool
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @return Pais
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * @param Pais $pais
     * @return Pais
     */
    public function setPais(Pais $pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * @return Contacto[]
     */
    public function getContactos()
    {
        return $this->contactos->toArray();
    }

    /**
     * @return OfertaAcademica
     */
    public function getOfertasAcademicas()
    {
        return $this->ofertasAcademicas->toArray();
    }

    public function __toString()
    {
        return $this->getCodigo().' - '.$this->getNombre();
    }
}

