<?php

namespace EPSA\TicketsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EPSA\TicketsBundle\Entity\Tickets;

class AdminTicketsController extends Controller
{
    /**
     * Lists all Tickets entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tickets = $em->getRepository('TicketsBundle:Tickets')->findAll();

        return $this->render('TicketsBundle:admin_tickets:index.html.twig', array(
            'tickets' => $tickets,
        ));
    }

    /**
     * Finds and displays a Tickets entity.
     *
     */
    public function showAction(Tickets $ticket)
    {
        if (($this->getUser() == $ticket->getUsuario()) or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {
            $resolveTicketForm = $this->createResolveTicketForm($ticket);
            $denyTicketForm = $this->createDenyTicketForm($ticket);
            $form = $this->createForm('EPSA\TicketsBundle\Form\TicketsReplyType', $ticket);

            return $this->render('TicketsBundle:admin_tickets:show.html.twig', array(
                'ticket' => $ticket,
                'form' => $form->createView(),
                'resolveTicket_form' => $resolveTicketForm->createView(),
                'denyTicket_form' => $denyTicketForm->createView(),
            ));
        }
        return $this->redirectToRoute('admin_ticket_index');
    }



    //MARCAR RESUELTO TICKET
    public function resolveTicketAction(Request $request, Tickets $ticket)
    {
        $form = $this->createResolveTicketForm($ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ticket->setUsuarioResolucion($this->getUser()->getEmail());
            $ticket->setFechaResolucion(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $ticket->setEstadoTicket("Resolved");

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Good job!',
                    'message' => 'The ticket was marked as resolved.'
                )
            );
        }

        return $this->redirectToRoute('admin_ticket_index');
    }


    private function createResolveTicketForm(Tickets $ticket)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_resolveTicket', array('id' => $ticket->getId())))
            ->getForm();
    }

    //MARCAR RECHAZADO
    public function denyTicketAction(Request $request, Tickets $ticket)
    {
        $form = $this->createDenyTicketForm($ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ticket->setUsuarioResolucion($this->getUser()->getEmail());
            $ticket->setFechaResolucion(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $ticket->setEstadoTicket("Rejected");

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Good job!',
                    'message' => 'The ticket was marked as rejected.'
                )
            );
        }
        return $this->redirectToRoute('admin_ticket_index');
    }

    private function createDenyTicketForm(Tickets $ticket)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_denyTicket', array('id' => $ticket->getId())))
            ->getForm();
    }

    public function replyTicketAction(Request $request, Tickets $ticket){
        $form = $this->createForm('EPSA\TicketsBundle\Form\TicketsReplyType', $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //OBTENEMOS LA RESPUESTA
            $respuesta = $form->get('respuesta')->getData();
            $ticket->setRespuesta($respuesta);
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Good job!',
                    'message' => 'Your reply has been added.'
                )
            );
        }
        return $this->render('TicketsBundle:admin_tickets:show.html.twig', array(
            'ticket' => $ticket,
            'form' => $form->createView(),
        ));
    }

}
