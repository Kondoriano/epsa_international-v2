<?php

namespace EPSA\UniversidadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use EPSA\UniversidadesBundle\Entity\Contacto;
use EPSA\UniversidadesBundle\Entity\Universidad;
use EPSA\UniversidadesBundle\Entity\Cargo;


class ContactoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Name',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => true
            ))
            ->add('apellidos', TextType::class, array(
                'label' => 'Surname',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => false
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => true
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Phone',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => false
            ))
            ->add('direccion', TextareaType::class, array(
                'label' => 'Address',
                'attr' => array(
                    'style' => 'resize:none',
                    'rows' => 6
                ),
                'required' => false
            ))
            ->add('cargo', EntityType::class, array(
                'label' => 'Position',
                // query choices from this entity
                'class' => Cargo::class,
                // use the User.username property as the visible option string
                'choice_label' => 'cargo',
                'placeholder' => 'Select an option',
                'required' => true
            ))
            ->add('universidad', EntityType::class, array(
                'label' => 'University',
                // query choices from this entity
                'class' => Universidad::class,
                // use the User.username property as the visible option string
                'choice_label' => 'nombre',
                'placeholder' => 'Select an option',
                'required' => true
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Contacto::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'epsa_universidadesbundle_contacto';
    }


}
