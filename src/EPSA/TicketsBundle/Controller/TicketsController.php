<?php

namespace EPSA\TicketsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EPSA\TicketsBundle\Entity\Tickets;
use EPSA\TicketsBundle\Form\TicketsType;

/**
 * Tickets controller.
 *
 */
class TicketsController extends Controller
{
    /**
     * Lists all Tickets entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tickets = $em->getRepository('TicketsBundle:Tickets')->findAll();

        return $this->render('TicketsBundle:tickets:index.html.twig', array(
            'tickets' => $tickets,
        ));
    }

    /**
     * Creates a new Tickets entity.
     *
     */
    public function newAction(Request $request)
    {
        $ticket = new Tickets();
        $ticket->setUsuario($this->getUser());
        $form = $this->createForm('EPSA\TicketsBundle\Form\TicketsType', $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Thank you!',
                    'message' => 'Your issue will be reviewed and you will be notified with the resolution.'
                )
            );

            return $this->redirectToRoute('ticket_index');
        }

        return $this->render('TicketsBundle:tickets:new.html.twig', array(
            'ticket' => $ticket,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Tickets entity.
     *
     */
    public function showAction(Tickets $ticket)
    {
        if (($this->getUser() == $ticket->getUsuario()) or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {
            $form = $this->createForm('EPSA\TicketsBundle\Form\TicketsReplyType', $ticket);

            return $this->render('TicketsBundle:tickets:show.html.twig', array(
                'ticket' => $ticket,
                'form' => $form->createView(),
            ));
        }
        return $this->redirectToRoute('ticket_index');
    }

}
