<?php

namespace EPSA\ExperienciasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EPSA\UniversidadesBundle\Entity\Universidad;
use Symfony\Component\Validator\Constraints as Assert;
use EPSA\UniversidadesBundle\Entity\Pais;
use AppBundle\Entity\Usuario;
use EPSA\EstudiosIdiomasBundle\Entity\Estudio;
use EPSA\EstudiosIdiomasBundle\Entity\Idioma;


/**
 * Experiencia
 *
 * @ORM\Table(name="experiencia")
 * @ORM\Entity(repositoryClass="EPSA\ExperienciasBundle\Repository\ExperienciaRepository")
 */
class Experiencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Universidad
     *
     * @ORM\ManyToOne(targetEntity="EPSA\UniversidadesBundle\Entity\Universidad", inversedBy="experiencias")
     * @ORM\JoinColumn(name="universidad_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $universidad;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario", inversedBy="experiencias")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @var Estudio
     *
     * @ORM\ManyToOne(targetEntity="EPSA\EstudiosIdiomasBundle\Entity\Estudio")
     * @Assert\NotBlank()
     */
    private $estudio;

    /**
     * @var Idioma
     *
     * @ORM\ManyToOne(targetEntity="EPSA\EstudiosIdiomasBundle\Entity\Idioma")
     * @Assert\NotBlank()
     */
    private $idioma;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the image.")
     * @Assert\Image()
     */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario_estudios", type="text")
     * @Assert\NotBlank()
     */
    private $comentarioEstudios;

    /**
     * @var int
     *
     * @ORM\Column(name="puntuacion_estudios", type="integer", nullable=false)
     * @ORM\Column(name="valoracion", type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      minMessage = "You must be at least {{ limit }}",
     *      maxMessage = "You cannot be taller than {{ limit }}"
     * )
     */
    private $puntuacionEstudios;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario_vida", type="text")
     * @Assert\NotBlank()
     */
    private $comentarioVida;

    /**
     * @var int
     *
     * @ORM\Column(name="puntuacion_vida", type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      minMessage = "You must be at least {{ limit }}",
     *      maxMessage = "You cannot be taller than {{ limit }}"
     * )
     */
    private $puntuacionVida;

    /**
     * @var int
     *
     * @ORM\Column(name="valoracion", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(value=0)
     */
    private $valoracion;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     * @Assert\Type(type="bool")
     */
    private $estado;

    /**
     * @var bool
     *
     * @ORM\Column(name="nueva", type="boolean", nullable=true)
     * @Assert\Type(type="bool")
     */
    private $nueva;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaPublicacion", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $fechaPublicacion;

    /**
     * Experiencia constructor.
     */
    public function __construct()
    {
        $this->nueva = true;
        $this->estado = false;
        $this->valoracion = 0;
        $this->fechaPublicacion = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set universidad
     *
     * @param string $universidad
     *
     * @return Experiencia
     */
    public function setUniversidad($universidad)
    {
        $this->universidad = $universidad;

        return $this;
    }

    /**
     * Get universidad
     *
     * @return string
     */
    public function getUniversidad()
    {
        return $this->universidad;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return Experiencia
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set comentarioEstudios
     *
     * @param string $comentarioEstudios
     *
     * @return Experiencia
     */
    public function setComentarioEstudios($comentarioEstudios)
    {
        $this->comentarioEstudios = $comentarioEstudios;

        return $this;
    }

    /**
     * Get comentarioEstudios
     *
     * @return string
     */
    public function getComentarioEstudios()
    {
        return $this->comentarioEstudios;
    }

    /**
     * Set comentarioVida
     *
     * @param string $comentarioVida
     *
     * @return Experiencia
     */
    public function setComentarioVida($comentarioVida)
    {
        $this->comentarioVida = $comentarioVida;

        return $this;
    }

    /**
     * Get comentarioVida
     *
     * @return string
     */
    public function getComentarioVida()
    {
        return $this->comentarioVida;
    }

    /**
     * Get estado
     *
     * @return bool
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set estado
     *
     * @param bool $estado
     *
     * @return Experiencia
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get nueva
     *
     * @return bool
     */
    public function getNueva()
    {
        return $this->nueva;
    }

    /**
     * Set nueva
     *
     * @param bool $nueva
     *
     * @return Experiencia
     */
    public function setNueva($nueva)
    {
        $this->nueva = $nueva;

        return $this;
    }

    /**
     * Get puntuacion estudios
     *
     * @return int
     */
    public function getPuntuacionEstudios()
    {
        return $this->puntuacionEstudios;
    }

    /**
     * Set puntuacion estudios
     *
     * @param int $puntuacionEstudios
     *
     * @return Experiencia
     *
     */
    public function setPuntuacionEstudios($puntuacionEstudios)
    {
        $this->puntuacionEstudios = $puntuacionEstudios;

        return $this;
    }

    /**
     * Get puntuacion vida
     *
     * @return int
     */
    public function getPuntuacionVida()
    {
        return $this->puntuacionVida;
    }

    /**
     * Set puntuacion vida
     *
     * @param int $puntuacionVida
     *
     * @return Experiencia
     */
    public function setPuntuacionVida($puntuacionVida)
    {
        $this->puntuacionVida = $puntuacionVida;

        return $this;
    }

    /**
     * Get valoracion
     *
     * @return int
     */
    public function getValoracion()
    {
        return $this->valoracion;
    }

    /**
     * Set valoracion
     *
     * @param int $valoracion
     *
     * @return Experiencia
     */
    public function setValoracion($valoracion)
    {
        $this->valoracion = $valoracion;

        return $this;
    }

    /**
     * Incrementar valoracion
     */
    public function incValoracion(){
        $this->valoracion = $this->valoracion + 1;
    }

    /**
     * Decrementar valoracion
     */
    public function decValoracion(){
        $this->valoracion = $this->valoracion - 1;
    }

    /**
     * @return Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set usuario
     *
     * @param Usuario $usuario
     *
     * @return Experiencia
     */
    public function setUsuario(Usuario $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return Estudio
     */
    public function getEstudio()
    {
        return $this->estudio;
    }

    /**
     * @param Estudio $estudio
     * @return Experiencia
     */
    public function setEstudio(Estudio $estudio)
    {
        $this->estudio = $estudio;

        return $this;
    }

    /**
     * @return Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * @param Idioma $idioma
     * @return Experiencia
     */
    public function setIdioma(Idioma $idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaPublicacion()
    {
        return $this->fechaPublicacion;
    }

    /**
     * @param mixed $fechaPublicacion
     */
    public function setFechaPublicacion($fechaPublicacion)
    {
        $this->fechaPublicacion = $fechaPublicacion;
    }
}

