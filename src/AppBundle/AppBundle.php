<?php
/**
 * Created by Fermín Martínez Gombao.
 * User: fermi
 * Date: 18/02/2017
 * Time: 20:30
 */

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
}
