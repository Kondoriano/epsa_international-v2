<?php

namespace EPSA\UniversidadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use EPSA\UniversidadesBundle\Entity\OfertaAcademica;
use EPSA\EstudiosIdiomasBundle\Entity\Idioma;
use EPSA\EstudiosIdiomasBundle\Entity\Estudio;
use EPSA\EstudiosIdiomasBundle\Entity\Nivel;

class OfertaAcademicaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comentario', TextareaType::class, array(
                'label' => 'Comment',
                'attr' => array(
                    'style' => 'resize:none',
                    'rows' => 2
                ),
                'required' => false
            ))
            ->add('estudio', EntityType::class, array(
                'label' => 'Study',
                // query choices from this entity
                'class' => Estudio::class,
                // use the User.username property as the visible option string
                'choice_label' => 'estudio',
                'placeholder' => 'Selecciona una opción',
                'required' => true
            ))
            ->add('idioma', EntityType::class, array(
                'label' => 'Language',
                // query choices from this entity
                'class' => Idioma::class,
                // use the User.username property as the visible option string
                'choice_label' => 'idioma',
                'placeholder' => 'Selecciona una opción',
                'required' => true
            ))
            ->add('nivel', EntityType::class, array(
                'label' => 'Level',
                // query choices from this entity
                'class' => Nivel::class,
                // use the User.username property as the visible option string
                'choice_label' => 'nivel',
                'placeholder' => 'Selecciona una opción',
                'required' => true
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OfertaAcademica::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'epsa_universidadesbundle_ofertaacademica';
    }


}
