<?php

namespace EPSA\ApuntesBundle\Controller;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;

use EPSA\ApuntesBundle\Entity\Apuntes;
use EPSA\ApuntesBundle\Form\ApuntesType;

/**
 * Apuntes controller.
 *
 */
class ApuntesController extends Controller
{
    /**
     * Lists all Apuntes entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $apuntes = $em->getRepository('ApuntesBundle:Apuntes')->findAll();

        return $this->render('ApuntesBundle:apuntes:index.html.twig', array(
            'apuntes' => $apuntes,
        ));
    }

    /**
     * Creates a new Apuntes entity.
     *
     */
    public function newAction(Request $request)
    {
        $apunte = new Apuntes();
        $apunte->setUsuario($this->getUser());
        $form = $this->createForm(ApuntesType::class, $apunte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $apunte->getNombreArchivo();

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            $file->move(
                $this->getParameter('apuntes_directory'),
                $fileName
            );

            $apunte->setNombreOriginal($file->getClientOriginalName());
            $apunte->setNombreArchivo($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($apunte);
            $em->flush();
            //FLASHBAG cuando se sube un archivo correctamente
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Congratulations!',
                    'message' => 'Your document has been uploaded!'
                )
            );

            return $this->redirect($this->generateUrl('apuntes_index'));
        }

        return $this->render('ApuntesBundle:apuntes:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Apuntes entity.
     *
     */
    public function showAction(Apuntes $apunte)
    {
        $deleteForm = $this->createDeleteForm($apunte);

        return $this->render('ApuntesBundle:apuntes:show.html.twig', array(
            'apunte' => $apunte,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Apuntes entity.
     *
     */

    public function editAction(Request $request, Apuntes $apunte)
    {
        if ($this->getUser() == $apunte->getUsuario()) {
            //PREPARACION PARA ELIMINAR EL VIEJO
            $deleteForm = $this->createDeleteForm($apunte);
            $fileSys = new Filesystem();
            $ruta = $this->getParameter('apuntes_directory');
            $nombreFichero = $apunte->getNombreArchivo();


            //PREPARACION PARA EDITAR EL VIEJO
            $editForm = $this->createForm('EPSA\ApuntesBundle\Form\ApuntesType', $apunte);
            $editForm->handleRequest($request);


            if ($editForm->isSubmitted() && $editForm->isValid()) {
                //NUEVO ARCHIVO SUBIDO
                $file = $apunte->getNombreArchivo();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $apunte->setNombreArchivo($fileName);
                $file->move(
                    $this->getParameter('apuntes_directory'),
                    $fileName
                );
                $apunte->setNombreOriginal($file->getClientOriginalName());

                //SE ELIMINA EL VIEJO
                $nombreCompleto = $ruta . '/' . $nombreFichero;
                $fileSys->remove($nombreCompleto);
                $deleteForm->handleRequest($request);

                //NUEVA FECHA
                $apunte->setFechaSubida(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($apunte);
                $em->flush();
                //FLASHBAG cuando se edita un archivo correctamente
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Congratulations!',
                        'message' => 'Your file has been updated.'
                    )
                );
                return $this->redirectToRoute('apuntes_show', array('id' => $apunte->getId()));
            }

            return $this->render('ApuntesBundle:apuntes:edit.html.twig', array(
                'apunte' => $apunte,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->redirectToRoute('apuntes_index');
        }
    }

    /**
     * Deletes a Apuntes entity.
     *
     */
    public function deleteConfirmAction(Apuntes $apunte)
    {
        if ($this->getUser() == $apunte->getUsuario()  or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {
            return $this->render('ApuntesBundle:apuntes:delete.html.twig', array(
                'apunte' => $apunte,
            ));
        }
        return $this->redirectToRoute('apuntes_index');
    }



    public function deleteAction(Request $request, Apuntes $apunte)
    {
        if (($this->getUser() == $apunte->getUsuario()) or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {

            $form = $this->createDeleteForm($apunte);
            $file = new Filesystem();
            $ruta = $this->getParameter('apuntes_directory');
            $nombreFichero = $apunte->getNombreArchivo();
            $nombreCompleto = $ruta . '/' . $nombreFichero;
            $file->remove($nombreCompleto);
            $form->handleRequest($request);

            $em = $this->getDoctrine()->getManager();
            $em->remove($apunte);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => '',
                    'message' =>  'Your file has been deleted.'
                )
            );
            return $this->redirectToRoute('apuntes_index');
        } else {
            return $this->redirectToRoute('apuntes_index');
        }
    }

    /**
     * Creates a form to delete a Apuntes entity.
     *
     * @param Apuntes $apunte The Apuntes entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Apuntes $apunte)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('apuntes_delete', array('id' => $apunte->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
