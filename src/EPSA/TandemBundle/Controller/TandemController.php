<?php

namespace EPSA\TandemBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EPSA\TandemBundle\Entity\Tandem;
use EPSA\TandemBundle\Form\TandemType;

/**
 * Tandem controller.
 *
 */
class TandemController extends Controller
{
    /**
     * Lists all Tandem entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tandems = $em->getRepository('TandemBundle:Tandem')->findAll();

        return $this->render('TandemBundle:tandem:index.html.twig', array(
            'tandems' => $tandems,
        ));
    }

    /**
     * Creates a new Tandem entity.
     *
     */
    public function newAction(Request $request)
    {
        $tandem = new Tandem();
        $tandem->setUsuario($this->getUser());
        $form = $this->createForm('EPSA\TandemBundle\Form\TandemType', $tandem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($tandem->getMaxPersonas() <= 1) {
                //FLASHBAG cuando hay error creando tandem.
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Atention!',
                        'message' => 'The maximum amounts of participants must be bigger than 1!'
                    )
                );
            } else {

                $em = $this->getDoctrine()->getManager();
                $em->persist($tandem);
                $em->flush();
                //FLASHBAG cuando tandem se crea correctamente
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Congratulations!',
                        'message' => 'Your Tandem has been created!'
                    )
                );
                return $this->redirectToRoute('tandem_index');
            }

        }
        return $this->render('TandemBundle:tandem:new.html.twig', array(
            'tandem' => $tandem,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Tandem entity.
     *
     */
    public function showAction(Tandem $tandem)
    {
        $deleteForm = $this->createDeleteForm($tandem);
        $addParticipationForm = $this->createAddParticipationForm($tandem);
        $removeParticipationForm = $this->createRemoveParticipationForm($tandem);
        $form = $this->createForm('EPSA\TandemBundle\Form\TandemType', $tandem);

        return $this->render('TandemBundle:tandem:show.html.twig', array(
            'tandem' => $tandem,
            'delete_form' => $deleteForm->createView(),
            'addParticipation_form' => $addParticipationForm->createView(),
            'removeParticipation_form' => $removeParticipationForm->createView(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Tandem entity.
     *
     */
    public function editAction(Request $request, Tandem $tandem)
    {
        if ($this->getUser() == $tandem->getUsuario()) {
            $deleteForm = $this->createDeleteForm($tandem);
            $editForm = $this->createForm('EPSA\TandemBundle\Form\TandemType', $tandem);
            $idiomaOriginal = $tandem->getIdiomaTandemAprender();
            $tandem->setIdiomaTandemAprender($idiomaOriginal);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                if ($tandem->getMaxPersonas() <= 1) {
                    //FLASHBAG cuando hay error editando tandem.
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        array(
                            'alert' => 'warning',
                            'title' => 'Atention!',
                            'message' => 'The maximum amounts of participants must be bigger than 1!'
                        )
                    );
                } else {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($tandem);
                    $em->flush();
                    //FLASHBAG cuando tandem se edita correctamente
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        array(
                            'alert' => 'success',
                            'title' => 'Congratulations!',
                            'message' => 'Your Tandem has been edited!'
                        )
                    );
                    return $this->redirectToRoute('tandem_show', array('id' => $tandem->getId()));
                }
            }

            return $this->render('TandemBundle:tandem:edit.html.twig', array(
                'tandem' => $tandem,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->redirectToRoute('tandem_index');
        }
    }

    /**
     * Deletes a Tandem entity.
     *
     */
    public function deleteConfirmAction(Tandem $tandem)
    {
        if ($this->getUser() == $tandem->getUsuario()  or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {
            return $this->render('TandemBundle:tandem:delete.html.twig', array(
                'tandem' => $tandem,
            ));
        }
        return $this->redirectToRoute('tandem_index');
    }


    public function deleteAction(Request $request, Tandem $tandem)
    {
        if ($this->getUser() == $tandem->getUsuario() or ($this->getUser()->hasRole('ROLE_ADMIN')) or ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')))  {
            $form = $this->createDeleteForm($tandem);
            $form->handleRequest($request);

            $em = $this->getDoctrine()->getManager();
            $em->remove($tandem);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Congratulations!',
                    'message' => 'Your Tandem has been deleted!'
                )
            );

        }
        return $this->redirectToRoute('tandem_index');

    }

    /**
     * Creates a form to delete a Tandem entity.
     *
     * @param Tandem $tandem The Tandem entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tandem $tandem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tandem_delete', array('id' => $tandem->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function addParticipationAction(Request $request, Tandem $tandem)
    {
        $form = $this->createAddParticipationForm($tandem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $participante = $this->getUser();

            /**$message = \Swift_Message::newInstance()
                ->setSubject('New Participant on your Tandem')
                ->setFrom('roman_registros@hotmail.com')
                ->setTo($tandem->getUsuario()->getEmail())
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'Emails/newParticipation.html.twig',
                        array('participante' => $participante,
                            'tandem' => $tandem
                        )
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
            */
            $participantes = $tandem->getPersonasApuntadas();
            $participantes++;
            $tandem->setPersonasApuntadas($participantes);

            $tandem_id = $tandem->getId();
            $usuario_id = $this->getUser();
            $tandemParticipante = $em->getRepository('TandemBundle:Tandem')->find($tandem_id);
            $usuarioParticipante = $em->getRepository('AppBundle:Usuario')->find($usuario_id);

            $tandemParticipante->addParticipante($usuarioParticipante);
            //$em->persist($tandemParticipante);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Congratulations!'  ,
                    'message' => 'Your participation has been added!'
                )
            );
        }
        return $this->redirectToRoute('tandem_index');

    }

    private function createAddParticipationForm(Tandem $tandem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tandem_addParticipation', array('id' => $tandem->getId())))
            //->setMethod('POST')
            ->getForm();
    }

    public function removeParticipationAction(Request $request, Tandem $tandem)
    {
        $form = $this->createRemoveParticipationForm($tandem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $participantes = $tandem->getPersonasApuntadas();
            $participantes--;
            $tandem->setPersonasApuntadas($participantes);

            $tandem_id = $tandem->getId();
            $usuario_id = $this->getUser();
            $tandemParticipante = $em->getRepository('TandemBundle:Tandem')->find($tandem_id);
            $usuarioParticipante = $em->getRepository('AppBundle:Usuario')->find($usuario_id);

            $tandemParticipante->removeParticipante($usuarioParticipante);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Good job,',
                    'message' => 'Your participation has been removed.'
                )
            );
        }
        return $this->redirectToRoute('tandem_index');
    }

    private function createRemoveParticipationForm(Tandem $tandem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tandem_removeParticipation', array('id' => $tandem->getId())))
            //->setMethod('POST')
            ->getForm();
    }

}
