<?php

namespace EPSA\UniversidadesBundle\Controller;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use EPSA\UniversidadesBundle\Entity\OfertaAcademica;
use EPSA\UniversidadesBundle\Entity\Universidad;
use EPSA\UniversidadesBundle\Form\OfertaAcademicaType;

/**
 * Ofertaacademica controller.
 *
 */
class OfertaAcademicaController extends Controller
{
    public function newAction(Request $request, Universidad $universidad)
    {
        $ofertaAcademica = new Ofertaacademica();
        $form = $this->createForm(OfertaAcademicaType::class, $ofertaAcademica);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $ofertaAcademica->setUniversidad($universidad);
                $em->persist($ofertaAcademica);
                $em->flush($ofertaAcademica);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'Academic offer has been created correctly.'
                    )
                );

                return $this->redirectToRoute('universidad_edit', array('id' => $universidad->getId()));
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => 'The academic offer you are trying to register already exists.'
                    )
                );
                $form->get('estudio')->addError(new FormError('Revise los campos.'));
                $form->get('idioma')->addError(new FormError('Revise los campos.'));
            } catch(PDOException $exception){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => 'There was an error creating the academic offer.'
                    )
                );
                $form->get('estudio')->addError(new FormError('Revise los campos.'));
                $form->get('idioma')->addError(new FormError('Revise los campos.'));
            } catch(DBALException $exception) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => 'There was an error creating the academic offer.'
                    )
                );
                $form->get('estudio')->addError(new FormError('Revise los campos.'));
                $form->get('idioma')->addError(new FormError('Revise los campos.'));
            }
        }

        return $this->render('UniversidadesBundle:ofertaacademica:new.html.twig', array(
            'universidad' => $universidad,
            'ofertaAcademica' => $ofertaAcademica,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'New'
        ));
    }

    /**
     * Displays a form to edit an existing ofertaAcademica entity.
     *
     */
    public function editAction(Request $request, OfertaAcademica $ofertaAcademica, Universidad $universidad)
    {
        $form = $this->createForm(OfertaAcademicaType::class, $ofertaAcademica);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The academic offer has been modified correctly.'
                    )
                );

                return $this->redirectToRoute('universidad_edit', array('id' => $universidad->getId()));
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => 'The academic offer you are trying to modify already exists.'
                    )
                );
                $form->get('estudio')->addError(new FormError('Revise los campos.'));
                $form->get('idioma')->addError(new FormError('Revise los campos.'));
            } catch(PDOException $exception){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => 'There was an error modifying the academic offer.'
                    )
                );
                $form->get('estudio')->addError(new FormError('Revise los campos.'));
                $form->get('idioma')->addError(new FormError('Revise los campos.'));
            } catch(DBALException $exception) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => 'There was an error modifying the academic offer. '
                    )
                );
                $form->get('estudio')->addError(new FormError('Revise los campos.'));
                $form->get('idioma')->addError(new FormError('Revise los campos.'));
            }
        }

        return $this->render('@Universidades/ofertaacademica/new.html.twig', array(
            'universidad' => $universidad,
            'ofertaAcademica' => $ofertaAcademica,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Edit'
        ));
    }

    /**
     * Deletes a oferta academica entity.
     *
     */
    public function deleteConfirmAction(Request $request, Universidad $universidad, OfertaAcademica $ofertaAcademica)
    {
        return $this->render('@Universidades/ofertaacademica/delete.html.twig', array(
            'ofertaAcademica' => $ofertaAcademica,
            'universidad' => $universidad,
            'action' => 'delete',
            'action_label' => 'Delete'
        ));
    }

    /**
     * Creates a form to delete a ofertaAcademica entity.
     *
     * @param OfertaAcademica $ofertaAcademica The ofertaAcademica entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function deleteAction(Request $request, Universidad $universidad, OfertaAcademica $ofertaAcademica)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ofertaAcademica);
            $em->flush($ofertaAcademica);
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'Academic offer has been successfully removed.'
                )
            );
        } catch(PDOException $exception){
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'warning',
                    'title' => 'Warning!',
                    'message' => 'There was an error removing the academic offer.'
                )
            );
        } catch(DBALException $exception) {
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => 'There was an error removing the academic offer.'
                )
            );
        }
        return $this->redirectToRoute('universidad_edit', array('id' => $universidad->getId()));
    }
}
