<?php

namespace EPSA\UniversidadesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use EPSA\UniversidadesBundle\Entity\Contacto;
use EPSA\UniversidadesBundle\Form\ContactoType;

/**
 * Contacto controller.
 *
 */
class ContactoController extends Controller
{
    /**
     * Lists all contacto entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if($request->query->has('query') and $request->query->get('query') != ""){
            $contactos = $em->getRepository('UniversidadesBundle:Contacto')->filterByQuery($request->query->get('query'));
        }else{
            $contactos = $em->getRepository('UniversidadesBundle:Contacto')->findAll();
        }

        return $this->render('@Universidades/contacto/index.html.twig', array(
            'contactos' => $contactos,
            'query' => $request->query->get('query')
        ));
    }

    /**
     * Creates a new contacto entity.
     *
     */
    public function newAction(Request $request)
    {
        $contacto = new Contacto();
        $form = $this->createForm(ContactoType::class, $contacto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contacto);
            $em->flush($contacto);

            return $this->redirectToRoute('contacto_index');
        }

        return $this->render('@Universidades/contacto/new.html.twig', array(
            'contacto' => $contacto,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'New'
        ));
    }

    /**
     * Displays a form to edit an existing contacto entity.
     *
     */
    public function editAction(Request $request, Contacto $contacto)
    {
        $form = $this->createForm(ContactoType::class, $contacto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contacto_edit', array('id' => $contacto->getId()));
        }

        return $this->render('@Universidades/contacto/new.html.twig', array(
            'contacto' => $contacto,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Edit'
        ));
    }

    /**
     * Render a template to confirm delete a contacto entity.
     *
     */
    public function deleteConfirmAction(Request $request, Contacto $contacto)
    {
        return $this->render('@Universidades/contacto/delete.html.twig', array(
            'contacto' => $contacto,
            'action' => 'delete',
            'action_label' => 'Delete'
        ));
    }

    public function deleteAction(Request $request, Contacto $contacto)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contacto);
            $em->flush($contacto);
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'The contact has been successfully deleted.'
                )
            );
        } catch (PDOException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        } catch (DBALException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        }

        return $this->redirectToRoute('contacto_index');
    }
}
