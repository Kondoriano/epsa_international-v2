<?php
/**
 * Created by PhpStorm.
 * User: Fermín Martínez Gombao
 * Date: 18/01/2017
 * Time: 23:03
 *
 * Clase que define un formulario Destino que servirá para el filtrado de Destinos. Este formulario se utilizará
 * sin vincular a ninguna clase, como si ocurre con formulario del tipo Universidad, Estudios...
 */

namespace EPSA\DestinosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

use EPSA\EstudiosIdiomasBundle\Entity\Estudio;
use EPSA\EstudiosIdiomasBundle\Entity\Idioma;
use EPSA\UniversidadesBundle\Entity\Pais;

class DestinosType extends AbstractType
{
    /**
     * Método que construye el formulario. Se definen los campos que tendrá.
     * Nótese como se hace uso de EntityType, para hacer referencia a que será un campo referente a una Entidad.
     * choice_label -> indica el texto que se mostrará en el desplegable, como se puede ver en el Idioma, se ha definido
     * una función para que muestre la combinación de idioma + nivel. Esto se hace porque por defecto en la variable
     * choice_label solo se puede asociar atributos de la entidad (idioma, nivel) de forma individual.
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estudio', EntityType::class, array(
                'label' => 'Study',
                'class' => Estudio::class,
                'choice_label' => 'estudio',
                'placeholder' => 'Select an option',
                'required' => false
            ))
            ->add('idioma', EntityType::class, array(
                'label' => 'Language',
                'class' => Idioma::class,
                'choice_label' => function ($idioma) {
                    return $idioma->getIdioma();
                },
                'placeholder' => 'Select an option',
                'required' => false
            ))
            ->add('pais', EntityType::class, array(
                'label' => 'Country',
                'class' => Pais::class,
                'choice_label' => 'nombre',
                'placeholder' => 'Select an option',
                'required' => false
            ))
        ;
    }
}
