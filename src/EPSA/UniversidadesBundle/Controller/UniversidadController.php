<?php

namespace EPSA\UniversidadesBundle\Controller;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use EPSA\UniversidadesBundle\Entity\Universidad;
use EPSA\UniversidadesBundle\Form\UniversidadType;

/**
 * Universidad controller.
 *
 */
class UniversidadController extends Controller
{
    /**
     * Lists all universidad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $universidades = $em->getRepository('UniversidadesBundle:Universidad')->findAll();

        return $this->render('@Universidades/universidad/index.html.twig', array(
            'universidades' => $universidades,
        ));
    }

    /**
     * Creates a new universidad entity.
     *
     */
    public function newAction(Request $request)
    {
        $universidad = new Universidad();
        $form = $this->createForm(UniversidadType::class, $universidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($universidad);
                $em->flush($universidad);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The university has been created correctly.'
                    )
                );

                return $this->redirectToRoute('universidad_edit', array('id' => $universidad->getId()));
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'A stored university already exists.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            }
        }

        return $this->render('@Universidades/universidad/new.html.twig', array(
            'universidad' => $universidad,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'New',
        ));
    }

    /**
     * Displays a form to edit an existing universidad entity.
     *
     */
    public function editAction(Request $request, Universidad $universidad)
    {
        $form = $this->createForm(UniversidadType::class, $universidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The university has been modified correctly.'
                    )
                );

                return $this->redirectToRoute('universidad_edit', array('id' => $universidad->getId()));
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'A stored university already exists.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Warning!',
                        'message' => $message
                    )
                );
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Danger!',
                        'message' => $message
                    )
                );
            }
        }

        return $this->render('@Universidades/universidad/edit.html.twig', array(
            'universidad' => $universidad,
            'form' => $form->createView(),
            'action' => 'edit',
            'action_label' => 'Edit',
        ));
    }

    /**
     * Render a template to confirm delete a universidad entity.
     *
     */
    public function deleteConfirmAction(Request $request, Universidad $universidad)
    {
        return $this->render('@Universidades/universidad/delete.html.twig', array(
            'universidad' => $universidad,
            'action' => 'delete',
            'action_label' => 'Delete',
        ));
    }

    public function deleteAction(Request $request, Universidad $universidad)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($universidad);
            $em->flush($universidad);
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'The university has been successfully removed.'
                )
            );
        } catch (PDOException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Daner!',
                    'message' => $message
                )
            );
        } catch (DBALException $exception) {
            $message = 'An error has occurred. (' . $exception->getMessage() . ')';
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Danger!',
                    'message' => $message
                )
            );
        }

        return $this->redirectToRoute('universidad_index');
    }
}
