<?php

namespace EPSA\ExperienciasBundle\Controller;

use EPSA\ExperienciasBundle\Entity\Experiencia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Experiencia controller.
 *
 */
class ExperienciaController extends Controller
{
    /**
     * Lists all experiencia entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->getUser()) {
            $experiencias = $em->getRepository('ExperienciasBundle:Experiencia')->filterExperiecias(
                $request->query->get('query'), $this->getUser(), null
            );
        } else {
            $experiencias = $em->getRepository('ExperienciasBundle:Experiencia')->findBy(array('estado' => true));
        }

        return $this->render('@Experiencias/experiencia/public_index.html.twig', array(
            'experiencias' => $experiencias,
            'query' => $request->query->get('query')
        ));
    }

    /**
     * Creates a new experiencia entity.
     *
     */
    public function newAction(Request $request)
    {
        $experiencia = new Experiencia();
        $form = $this->createForm('EPSA\ExperienciasBundle\Form\ExperienciaType', $experiencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();

                // $file stores the uploaded PDF file
                $file = $experiencia->getImagen();

                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('experiencias_directory'),
                    $fileName
                );

                // Update the 'brochure' property to store the PDF file name
                // instead of its contents
                $experiencia->setImagen($fileName);
                $experiencia->setUsuario($this->getUser());

                $em->persist($experiencia);
                $em->flush($experiencia);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The experience has been created correctly.'
                    )
                );

                return $this->redirectToRoute('experiencia_index');
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                $message = 'A stored experience already exists.';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'Attention!',
                        'message' => $message
                    )
                );
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Error!',
                        'message' => $message
                    )
                );
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Error!',
                        'message' => $message
                    )
                );
            }
        }

        return $this->render('@Experiencias/experiencia/new.html.twig', array(
            'experiencia' => $experiencia,
            'form' => $form->createView(),
            'action' => 'new',
            'action_label' => 'New'
        ));
    }

    /**
     * Finds and displays a experiencium entity.
     *
     */
    public function showAction(Experiencia $experiencia)
    {
        /* Buscamos si existe la esperiencia para poder renderizarla */
        $em = $this->getDoctrine()->getManager();

        $experiencia = $em->getRepository('ExperienciasBundle:Experiencia')->findOneBy(array('id' => $experiencia));

        if ($experiencia != null) {
            return $this->render('ExperienciasBundle:experiencia:show.html.twig', array(
                'experiencia' => $experiencia,
                'action' => 'show',
                'action_label' => 'View'
            ));
        } else {
            return $this->redirectToRoute('experiencia_index');
        }
    }

    /**
     * Displays a form to edit an existing experiencium entity.
     *
     */
    public function editAction(Request $request, Experiencia $experiencia)
    {
        /* Comprobar que la experiencia que se pretende editar pertenece al usuario activo */
        if ($experiencia->getUsuario() == $this->getUser()) {

            $form = $this->createForm('EPSA\ExperienciasBundle\Form\ExperienciaType', $experiencia);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                try {
                    $this->getDoctrine()->getManager()->flush();

                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        array(
                            'alert' => 'success',
                            'title' => 'Success!',
                            'message' => 'The experience has been modified correctly.'
                        )
                    );

                    return $this->redirectToRoute('experiencia_index');
                } catch (UniqueConstraintViolationException $constraintViolationException) {
                    $message = 'Ya existe una experiencia almacenada.';
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        array(
                            'alert' => 'warning',
                            'title' => 'Attention!',
                            'message' => $message
                        )
                    );
                } catch (PDOException $exception) {
                    $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        array(
                            'alert' => 'danger',
                            'title' => 'Error!',
                            'message' => $message
                        )
                    );
                } catch (DBALException $exception) {
                    $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        array(
                            'alert' => 'danger',
                            'title' => 'Error!',
                            'message' => $message
                        )
                    );
                }
            }

            return $this->render('@Experiencias/experiencia/new.html.twig', array(
                'experiencia' => $experiencia,
                'form' => $form->createView(),
                'action' => 'edit',
                'action_label' => 'Edit'
            ));
        } else {
            return $this->redirectToRoute('experiencia_index');
        }
    }

    /**
     * Render a template to confirm delete a Exoeriencia entity.
     *
     */
    public function deleteConfirmAction(Request $request, Experiencia $experiencia)
    {
        if ($experiencia->getUsuario() == $this->getUser()) {
            return $this->render('ExperienciasBundle:experiencia:delete.html.twig', array(
                'experiencia' => $experiencia,
                'action' => 'delete',
                'action_label' => 'Delete'
            ));
        } else {
            return $this->redirectToRoute('experiencia_index');
        }
    }

    public function deleteAction(Request $request, Experiencia $experiencia)
    {
        if ($experiencia->getUsuario() == $this->getUser()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($experiencia);
                $em->flush($experiencia);
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Success!',
                        'message' => 'The experience has been successfully deleted.'
                    )
                );
            } catch (PDOException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Error!',
                        'message' => $message
                    )
                );
            } catch (DBALException $exception) {
                $message = 'An error has occurred. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Error!',
                        'message' => $message
                    )
                );
            }
        }

        return $this->redirectToRoute('experiencia_index');
    }
}
