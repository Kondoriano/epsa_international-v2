<?php
/**
 * Created by PhpStorm.
 * User: fermi
 * Date: 20/05/2017
 * Time: 12:51
 */

namespace EPSA\UniversidadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

use EPSA\UniversidadesBundle\Entity\Cargo;

class MailingType extends AbstractType
{
    /**
     * Método que construye el formulario. Se definen los campos que tendrá.
     * Nótese como se hace uso de EntityType, para hacer referencia a que será un campo referente a una Entidad.
     * choice_label -> indica el texto que se mostrará en el desplegable, como se puede ver en el Idioma, se ha definido
     * una función para que muestre la combinación de idioma + nivel. Esto se hace porque por defecto en la variable
     * choice_label solo se puede asociar atributos de la entidad (idioma, nivel) de forma individual.
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', EntityType::class, array(
                'label' => 'Position',
                'class' => Cargo::class,
                'choice_name' => 'cargo',
                'choice_value' => 'id',
                'placeholder' => '----- All -----',
                'required' => false
            ))
            ->add('content', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
        ;
    }
}