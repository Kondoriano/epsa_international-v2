<?php

namespace EPSA\ConvocatoriasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Convocatoria
 *
 * @ORM\Table(name="convocatoria")
 * @ORM\Entity(repositoryClass="EPSA\ConvocatoriasBundle\Repository\ConvocatoriaRepository")
 */
class Convocatoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, unique=true)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="text", nullable=true)
     */
    private $comentario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="date", nullable=true)
     */
    private $fechaFin;

    /**
     * @var int
     *
     * @ORM\Column(name="totalUniversidades", type="integer", nullable=true)
     */
    private $totalUniversidades;

    /**
     * @var int
     *
     * @ORM\Column(name="plazasOcupadas", type="integer", nullable=true)
     */
    private $plazasOcupadas;

    /**
     * @var int
     *
     * @ORM\Column(name="plazasConvocadas", type="integer", nullable=true)
     */
    private $plazasConvocadas;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Convocatoria
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return Convocatoria
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     *
     * @return Convocatoria
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     *
     * @return Convocatoria
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set totalUniversidades
     *
     * @param integer $totalUniversidades
     *
     * @return Convocatoria
     */
    public function setTotalUniversidades($totalUniversidades)
    {
        $this->totalUniversidades = $totalUniversidades;

        return $this;
    }

    /**
     * Get totalUniversidades
     *
     * @return int
     */
    public function getTotalUniversidades()
    {
        return $this->totalUniversidades;
    }

    /**
     * Set plazasOcupadas
     *
     * @param integer $plazasOcupadas
     *
     * @return Convocatoria
     */
    public function setPlazasOcupadas($plazasOcupadas)
    {
        $this->plazasOcupadas = $plazasOcupadas;

        return $this;
    }

    /**
     * Get plazasOcupadas
     *
     * @return int
     */
    public function getPlazasOcupadas()
    {
        return $this->plazasOcupadas;
    }

    /**
     * Set plazasConvocadas
     *
     * @param integer $plazasConvocadas
     *
     * @return Convocatoria
     */
    public function setPlazasConvocadas($plazasConvocadas)
    {
        $this->plazasConvocadas = $plazasConvocadas;

        return $this;
    }

    /**
     * Get plazasConvocadas
     *
     * @return int
     */
    public function getPlazasConvocadas()
    {
        return $this->plazasConvocadas;
    }
}

