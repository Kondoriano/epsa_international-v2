<?php

namespace EPSA\TicketsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tickets")
 * @ORM\Entity
 */

class Tickets{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $titulo;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $apartado;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $estado_ticket = "Pending";

    /**
     * @ORM\Column(type="text")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $respuesta;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha_creacion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha_resolucion = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $usuario_resolucion = null;

    //Relación N:1 entre la tabla Tickets y Usuario
    //Clave ajena para guardar al creador del ticket
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario", inversedBy="tickets")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;




    public function __toString()
    {
        return (String)$this->getId();
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fecha_creacion = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Tickets
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set apartado
     *
     * @param string $apartado
     *
     * @return Tickets
     */
    public function setApartado($apartado)
    {
        $this->apartado = $apartado;

        return $this;
    }

    /**
     * Get apartado
     *
     * @return string
     */
    public function getApartado()
    {
        return $this->apartado;
    }

    /**
     * Set estadoTicket
     *
     * @param string $estadoTicket
     *
     * @return Tickets
     */
    public function setEstadoTicket($estadoTicket)
    {
        $this->estado_ticket = $estadoTicket;

        return $this;
    }

    /**
     * Get estadoTicket
     *
     * @return string
     */
    public function getEstadoTicket()
    {
        return $this->estado_ticket;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Tickets
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set respuesta
     *
     * @param string $respuesta
     *
     * @return Tickets
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return string
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }


    /**
     * Set fechaCreacion
     *
     * @param \datatime $fechaCreacion
     *
     * @return Tickets
     */
    public function setFechaCreacion(\datetime $fechaCreacion)
    {
        $this->fecha_creacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \datatime
     */
    public function getFechaCreacion()
    {
        return $this->fecha_creacion;
    }

    /**
     * Set fechaResolucion
     *
     * @param \datatime $fechaResolucion
     *
     * @return Tickets
     */
    public function setFechaResolucion(\datetime $fechaResolucion = null)
    {
        $this->fecha_resolucion = $fechaResolucion;

        return $this;
    }

    /**
     * Get fechaResolucion
     *
     * @return \datatime
     */
    public function getFechaResolucion()
    {
        return $this->fecha_resolucion;
    }

    /**
     * Set usuarioResolucion
     *
     * @param string $usuarioResolucion
     *
     * @return Tickets
     */
    public function setUsuarioResolucion($usuarioResolucion)
    {
        $this->usuario_resolucion = $usuarioResolucion;

        return $this;
    }

    /**
     * Get usuarioResolucion
     *
     * @return string
     */
    public function getUsuarioResolucion()
    {
        return $this->usuario_resolucion;
    }



    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     *
     * @return Tickets
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
