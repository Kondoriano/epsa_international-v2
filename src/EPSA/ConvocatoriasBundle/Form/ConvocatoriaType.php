<?php

namespace EPSA\ConvocatoriasBundle\Form;

use EPSA\ConvocatoriasBundle\Entity\Convocatoria;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ConvocatoriaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', TextType::class, array(
                'label' => 'Title',
                'attr' => array(
                    'placeholder' => ''
                ),
                'required' => true
            ))
            ->add('comentario', TextareaType::class, array(
                'label' => 'Comment',
                'attr' => array(
                    'style' => 'resize:none',
                    'rows' => 2
                ),
                'required' => false
            ))
            ->add('plazasOcupadas', IntegerType::class, array(
                'label' => 'Occupied squares',
                'required' => false,
                'attr' => array(
                    'min' => 0,
                )
            ))
            ->add('fechaInicio', DateType::class, array(
                'label' => 'Start date',
                'widget' => 'single_text',
                'html5' => 'true',
                'required' => false
            ))
            ->add('fechaFin', DateType::class, array(
                'label' => 'End date',
                'widget' => 'single_text',
                'html5' => 'true',
                'required' => false
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Convocatoria::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'epsa_convocatoriasbundle_convocatoria';
    }


}
