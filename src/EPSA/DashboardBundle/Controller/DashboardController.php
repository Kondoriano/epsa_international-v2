<?php

namespace EPSA\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    /**
     * Métopdo que presenta informacione en el dashboard de las diversas partes de la aplicacion
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $nuevas_experiencias = $em->getRepository('ExperienciasBundle:Experiencia')->findBy(array('nueva'=>true));

        //Total universidades: ofertadas y ocultas
        $universidades = count($em->getRepository('UniversidadesBundle:Universidad')->findBy(array('estado'=>true)));
        $universidades_ocultas = count($em->getRepository('UniversidadesBundle:Universidad')->findBy(array('estado'=>false)));

        //Total plazas ofertadas
        $plazas_ofertadas = $em->getRepository('UniversidadesBundle:Universidad')->plazasConvocadas(true);
        $plazas_no_ofertadas = $em->getRepository('UniversidadesBundle:Universidad')->plazasConvocadas(false);

        //Total experiencias: publicados y pendientes
        $total_experiencias = count($em->getRepository('ExperienciasBundle:Experiencia')->findAll());
        $texp_cweek = 13;
        $texp_lweek = 12;


        //Total tandems:


        return $this->render('DashboardBundle:Dashboard:index.html.twig', array(
            'nuevas_experiencias' => $nuevas_experiencias,
            'universidades' => $universidades,
            'universidades_ocultas' => $universidades_ocultas,
            'plazas_ofertadas' => $plazas_ofertadas,
            'plazas_no_ofertadas' => $plazas_no_ofertadas,
            'total_experiencias' => $total_experiencias,
            'texp_cweek' => $texp_cweek,
            'texp_lweek' => $texp_lweek,
        ));
    }
}
