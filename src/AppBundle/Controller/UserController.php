<?php
/**
 * Created by Fermín Martínez Gombao.
 * User: fermi
 * Date: 18/02/2017
 * Time: 20:30
 */

namespace AppBundle\Controller;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Usuario;
use AppBundle\Form\AdminEditUserFormType;


/**
 * Class UserController.
 * Sirve para las funciones administrativas de los usuarios: listado y modificación de roles y activación de usuarios.
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    /**
     * Listado de los usuarios para la administración por parte de los super administradores.
     * @Route("/admin/usuarios", name="usuarios_index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        //$usuarios = $this->get('fos_user.user_manager')->findUsers();

        $usuarios = $this->getDoctrine()->getRepository('AppBundle:Usuario')->filterDistictCurrentUser($this->getUser());

        // replace this example code with whatever you need
        return $this->render('usuarios/index.html.twig', array(
            'usuarios' => $usuarios
        ));
    }

    /**
     * Modificación de roles y activación de un usuario por parte de los super administradores.
     * @Route("/admin/usuarios/edit/{id}", name="usuarios_edit")
     */
    public function editAction(Request $request, Usuario $usuario)
    {
        $form = $this->createForm(AdminEditUserFormType::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form['role']->getData();

                $em = $this->getDoctrine()->getManager();
                //Borramos todos los role asociados
                $usuario->setRoles(array());
                //Se asigna el nuevo Rol
                $usuario->addRole($data);
                $em->persist($usuario);
                $em->flush($usuario);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'Correcto!',
                        'message' => 'El usuario ha sido modificado correctamente.'
                    )
                );

                return $this->redirectToRoute('usuarios_index');
            } catch (PDOException $exception) {
                $message = 'Se ha producido un error. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Error!',
                        'message' => $message
                    )
                );
            } catch (DBALException $exception) {
                $message = 'Se ha producido un error. (' . $exception->getMessage() . ')';
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'danger',
                        'title' => 'Error!',
                        'message' => $message
                    )
                );
            }
        }

        return $this->render('usuarios/edit.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        ));
    }
}