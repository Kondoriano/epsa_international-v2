<?php

namespace EPSA\UniversidadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint as UNICIDAD;
use Symfony\Component\Validator\Constraints as Assert;

use EPSA\EstudiosIdiomasBundle\Entity\Estudio;
use EPSA\EstudiosIdiomasBundle\Entity\Idioma;
use EPSA\EstudiosIdiomasBundle\Entity\Nivel;

/**
 * OfertaAcademica
 *
 * @ORM\Table(
 *     name="oferta_academica",
 *     uniqueConstraints={@UNICIDAD(name="unicidad", columns={"estudio_id", "idioma_id", "universidad_id"})}
 * )
 * @ORM\Entity(repositoryClass="EPSA\UniversidadesBundle\Repository\OfertaAcademicaRepository")
 */
class OfertaAcademica
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="text", nullable=true)
     */
    private $comentario;

    /**
     * @var Estudio
     *
     * @ORM\ManyToOne(targetEntity="EPSA\EstudiosIdiomasBundle\Entity\Estudio")
     * @Assert\NotBlank()
     */
    private $estudio;

    /**
     * @var Idioma
     *
     * @ORM\ManyToOne(targetEntity="EPSA\EstudiosIdiomasBundle\Entity\Idioma")
     * @Assert\NotBlank()
     */
    private $idioma;

    /**
     * @var Nivel
     *
     * @ORM\ManyToOne(targetEntity="EPSA\EstudiosIdiomasBundle\Entity\Nivel")
     * @Assert\NotBlank()
     */
    private $nivel;

    /**
     * @var Universidad
     *
     * @ORM\ManyToOne(targetEntity="Universidad", inversedBy="ofertasAcademicas")
     * @ORM\JoinColumn(name="universidad_id", referencedColumnName="id")
     */
    private $universidad;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return OfertaAcademica
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * @return Estudio
     */
    public function getEstudio()
    {
        return $this->estudio;
    }

    /**
     * @param Estudio $estudio
     * @return OfertaAcademica
     */
    public function setEstudio(Estudio $estudio)
    {
        $this->estudio = $estudio;

        return $this;
    }

    /**
     * @return Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * @param Idioma $idioma
     * @return OfertaAcademica
     */
    public function setIdioma(Idioma $idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * @return Nivel
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param Nivel $nivel
     * @return OfertaAcademica
     */
    public function setNivel(Nivel $nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * @return Universidad
     */
    public function getUniversidad()
    {
        return $this->universidad;
    }

    /**
     * @param Universidad $universidad
     * @return OfertaAcademica
     */
    public function setUniversidad(Universidad $universidad)
    {
        $this->universidad = $universidad;

        return $this;
    }

    public function __toString()
    {
        return $this->getEstudio()->getEstudio().' - '.$this->getIdioma()->getIdioma().' - '.$this->getComentario();
    }
}

