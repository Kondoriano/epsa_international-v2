<?php
/**
 * Created by Fermín Martínez Gombao.
 * User: fermi
 * Date: 18/02/2017
 * Time: 20:30
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsuarioRepository")
 */
class Usuario extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column (type="string", length=100, nullable=true)
     */
    protected $nombre;

    /**
     * @ORM\Column (type="string", length=200, nullable=true)
     */
    protected $apellido;

    /**
     * @ORM\Column (type="string", length=9, nullable=true)
     *
     * @Assert\Regex(
     *     pattern="/\d{9}/",
     *     message="The phone number isn't valid."
     * )
     */
    protected $telefono;

    /**
     * @ORM\Column (type="string", length=20, nullable=true)
     *
     * @Assert\Choice({"male", "female"})
     *
     */
    protected $sexo;

    /**
     * @ORM\Column (type="string", length=150, nullable=true)
     *
     * @Assert\Choice({"erasmus", "student"})
     *
     */
    protected $procedencia_usuario;

    /**
     * @ORM\Column (type="datetime")
     *
     * @Assert\DateTime()
     */
    protected $fecha_registro;


    //Atributo para mapear la clave ajena de usuarios que crean tandem (Entidad Tandem)
    /**
     * @ORM\OneToMany(targetEntity="EPSA\TandemBundle\Entity\Tandem", mappedBy="usuario")
     */
    private $tandems;


    //Atributo para mapear la clave ajena de participantes (Entidad Tandem)
    /**
     * @ORM\ManyToMany(targetEntity="EPSA\TandemBundle\Entity\Tandem", mappedBy="participantes")
     */
    private $usuarios;

    //Atributo para mapear la clave ajena de usuarios que crean el ticket (Entidad Tickets)
    /**
     * @ORM\OneToMany(targetEntity="EPSA\TicketsBundle\Entity\Tickets", mappedBy="usuario")
     */
    private $tickets;

    //Atributo para mapear la clave ajena de usuarios que suben archivos (Entidad Apuntes)
    /**
     * @ORM\OneToMany(targetEntity="EPSA\ApuntesBundle\Entity\Apuntes", mappedBy="usuario")
     */
    private $archivos;

    public function __construct()
    {
        $this->fecha_registro = new \DateTime();
        $this->archivos = new ArrayCollection();
        $this->tandems = new ArrayCollection();
        $this->roles = array('ROLE_USER');
    }

    public function __toString(){
        return (String)$this->getId();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @param mixed $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getProcedenciaUsuario()
    {
        return $this->procedencia_usuario;
    }

    /**
     * @param mixed $procedencia_usuario
     */
    public function setProcedenciaUsuario($procedencia_usuario)
    {
        $this->procedencia_usuario = $procedencia_usuario;
    }

    /**
     * @return mixed
     */
    public function getFechaRegistro()
    {
        return $this->fecha_registro;
    }

    /**
     * @param mixed $fecha_registro
     */
    public function setFechaRegistro($fecha_registro)
    {
        $this->fecha_registro = $fecha_registro;
    }



    /**  Methods for tandems */
    /**
     * Add tandem
     *
     * @param \EPSA\TandemBundle\Entity\Tandem $tandem
     *
     * @return Usuario
     */
    public function addTandem(\EPSA\TandemBundle\Entity\Tandem $tandem)
    {
        $this->tandems[] = $tandem;

        return $this;
    }

    /**
     * Remove tandem
     *
     * @param \EPSA\TandemBundle\Entity\Tandem $tandem
     */
    public function removeTandem(\EPSA\TandemBundle\Entity\Tandem $tandem)
    {
        $this->tandems->removeElement($tandem);
    }

    /**
     * Get tandems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTandems()
    {
        return $this->tandems;
    }

    /**
     * Add usuario
     *
     * @param \EPSA\TandemBundle\Entity\Tandem $usuario
     *
     * @return Usuario
     */
    public function addUsuario(\EPSA\TandemBundle\Entity\Tandem $usuario)
    {
        $this->usuarios[] = $usuario;

        return $this;
    }

    /**
     * Remove usuario
     *
     * @param \EPSA\TandemBundle\Entity\Tandem $usuario
     */
    public function removeUsuario(\EPSA\TandemBundle\Entity\Tandem $usuario)
    {
        $this->usuarios->removeElement($usuario);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /** END OF METHODS FOR TANDEMS */

    /** METHODS FOR TICKETS */

    /**
     * @return mixed
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param mixed $tickets
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;
    }

    /** END OF METHODS FOR TICKETS */

    /** METHODS FOR FILES */
    /**
     * @return mixed
     */
    public function getArchivos()
    {
        return $this->archivos;
    }

    /**
     * @param mixed $archivos
     */
    public function setArchivos($archivos)
    {
        $this->archivos = $archivos;
    }

    /** END OF METHODS FOR FILES */
}

