Proyecto EPSA Internacional
=========

Proyecto final de:
 - Román Bas Bas
 - Fermín Martínez Gombao
 
## Estructura del proyecto
El proyecto tendrá la siguiente estrucutra:
 - AppBundle: Bundle donde se situará información básica tales como todas las _Entidades_ y la parte del _homepage_ 
 que será lo más básico para el funcionamiento de la aplicación.
 
 - **Admin**: Administración solo para administradoreS.
 
 - **Intranet**: intranet para que los alumnos gestionen su información.
 
 - **Frontend**: parte pública sin necesidad de _login_
 
## Gestión de usuarios
Para esta tarea se ha implementado un Bundle de terceros llamado _FSOUSERBUNDLE_ cuya documentación se encuentra dentro
de la propia web de _SYMFONY_: http://symfony.com/doc/current/bundles/FOSUserBundle/index.html

Las ventajas que ofrece emplear este tipo de Bundle es que ya implementa muchas tareas que de lo contrarío habrñia que 
desarrollar desde cero, como son:
- Registro de usuarios con activacion por correo.
- Login.
- Edición del perfil de usuario.
- Recuperación de contraseña mediante envio de correo electrónico.
- ...

## Comandos Symfony importantes

- Generación de nuevo Bundle: 
```generate:bundle --namespace=EPSA/ExperienciasBundle --bundle-name=ExperienciasBundle --dir=src/ --format=yml --no-interaction```

- Generación de nueva Entity:
```symfony doctrine:generate:entity```

- Actualizar Base de Datos después de crear nueva Entity
```symfony doctrine:schema:update```

- Generar CRUD a partir de Entity:
```symfony doctrine:generate:crud --entity=ConvocatoriasBundle:Convocatoria --route-prefix=convocatoria --with-write --format=yml --no-interaction```

