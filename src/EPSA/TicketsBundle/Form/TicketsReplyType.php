<?php

namespace EPSA\TicketsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TicketsReplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('respuesta', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Write a reply',
                    'requiered' => true,
                    'oninvalid' => "setCustomValidity('You must write a reply before resolve or reject the ticket.')",
                    'oninput' => "setCustomValidity('')",
                    'style' => 'width:100%'
                )
            ))
        ;
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EPSA\TicketsBundle\Entity\Tickets'
        ));
    }
}
