<?php

namespace EPSA\ExperienciasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use EPSA\UniversidadesBundle\Entity\Universidad;
use EPSA\EstudiosIdiomasBundle\Entity\Estudio;
use EPSA\EstudiosIdiomasBundle\Entity\Idioma;


class ExperienciaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('universidad', EntityType::class, array(
                'label' => 'University',
                // query choices from this entity
                'class' => Universidad::class,
                // use the User.username property as the visible option string
                'choice_label' => 'nombre',
                'placeholder' => 'Select an option',
                'required' => true,
            ))
            ->add('estudio', EntityType::class, array(
                'label' => 'Study',
                // query choices from this entity
                'class' => Estudio::class,
                // use the User.username property as the visible option string
                'choice_label' => 'estudio',
                'placeholder' => 'Select an option',
                'required' => true
            ))
            ->add('idioma', EntityType::class, array(
                'label' => 'Language',
                // query choices from this entity
                'class' => Idioma::class,
                // use the User.username property as the visible option string
                'choice_label' => function ($idioma) {
                    return $idioma->__toString();
                },
                'placeholder' => 'Select an option',
                'required' => true
            ))
            ->add('imagen', FileType::class, array(
                'label' => 'Imge',
                'data_class' => null
            ))
            ->add('comentarioEstudios', TextareaType::class, array(
                'label' => 'Comments about study',
                'attr' => array(
                    'style' => 'resize:none',
                    'rows' => 6
                ),
                'required' => true
            ))
            ->add('puntuacionEstudios', IntegerType::class, array(
                'label' => 'Score on the level of studies',
                'attr' => array(
                    'min' => '0',
                    'max' => '5'
                ),
                'required' => true
            ))
            ->add('comentarioVida', TextareaType::class, array(
                'label' => 'Life level comment',
                'attr' => array(
                    'style' => 'resize:none',
                    'rows' => 6
                ),
                'required' => true
            ))
            ->add('puntuacionVida', IntegerType::class, array(
                'label' => 'Life level score',
                'attr' => array(
                    'min' => 0,
                    'max' => 5
                ),
                'required' => true
            ))
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EPSA\ExperienciasBundle\Entity\Experiencia'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'epsa_experienciasbundle_experiencia';
    }


}
